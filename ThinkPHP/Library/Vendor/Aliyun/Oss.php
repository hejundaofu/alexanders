<?php
require_once 'aliyun.php';

use Aliyun\OSS\OSSClient;

class Oss{
    private $keyId = '4NaUbM8ehMYYzgPD';
    private $keySecret = 'g1HLdUnqM2CJdbWlQtGrkcOZrZJzzP';
    private $endpoint = 'http://oss-cn-qingdao.aliyuncs.com';
    private $url = 'http://img.msa12365.com/';

    private $bucket;
    private $size;

    private $client;

    public function __construct($bucket){
        $this->bucket = $bucket;
        $client = $this->createClient();
    }

    public function uploadFile($client, $fileInfo, $oldFileName){
        $req = $this->putResourceObject($client, $fileInfo);

        // if($oldFileName)
        //     $this->deleteObject($client, $oldFileName);

        return $req;
    }

    // Sample of create client
    public function createClient() {
        return OSSClient::factory(array(
            'AccessKeyId' => $this->keyId,
            'AccessKeySecret' => $this->keySecret,
            'Endpoint' => $this->endpoint
        ));
    }

    private function listObjects() {
        $result = $client->listObjects(array(
            'Bucket' => $this->bucket,
        ));
        foreach ($result->getObjectSummarys() as $summary) {
            echo 'Object key: ' . $summary->getKey() . "\n";
        }
    }

    // Sample of put object from string
    private function putStringObject() {
        $result = $this->client->putObject(array(
            'Bucket' => $this->bucket,
            'Key' => $this->key,
            'Content' => $content,
        ));
        echo 'Put object etag: ' . $result->getETag();
    }

    // Sample of put object from resource
    private function putResourceObject(OSSClient $client, $fileInfo) {
        $strPath = './Public/uploads/' . $fileInfo['savename'];
        $content = fopen($strPath, 'r');
        $size = filesize($strPath);

        $fileName = $fileInfo['md5']. '.' . $fileInfo['ext'];

        $result = $client->putObject(array(
            'Bucket' => $this->bucket,
            'Key' => $fileName,
            'Content' => $content,
            'ContentLength' => $size,
            'ContentType' => $fileInfo['type'],
            'ContentDisposition' => "inline;filename={$fileName}"
        ));

        fclose($content);

        if($result->getETag()){
            $req['code'] = 0;
            $req['imgUrl'] = $this->url . $fileName;
            return $req;
        }else{
            $req['code'] = -1;
        }
        return $req;
    }

    // Sample of get object
    private function getObject() {
        $object = $client->getObject(array(
            'Bucket' => $this->bucket,
            'Key' => $this->key,
        ));

        echo "Object: " . $object->getKey() . "\n";
        echo (string) $object;
    }

    // Sample of delete object
    private function deleteObject($client, $key) {
        return $client->deleteObject(array(
            'Bucket' => $this->bucket,
            'Key' => $key,
        ));
    }
}