<?php

namespace Org\Szscjg;
class SzscjgClient{
	private $service = 'http://app03.szaic.gov.cn/szscjgWapWebsrv/CreditService.asmx';
	private $token = 'szcredit';
	private $timestamp;
	private $nonce;
	private $signature;

	private $soap;

	public function __construct() {
		$this->timestamp = time()-200;
		$this->nonce = rand();

		$tmpArr = array($this->token, $this->timestamp, $this->nonce);

		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );

		$this->signature = sha1( $tmpStr );
    }

    private function createSoap(){
    	Vendor( 'NuSoap.nusoap' );
        $this->soap = new \nusoap_client( $this->WebServiceURL, true, false, false, false, false, 0, 30 );
        $this->soap->soap_defencoding = 'UTF-8';
        $this->soap->decode_utf8 = false;
        $this->soap->xml_encoding = 'utf-8';
    }

    public function GetEnterpriseListByNameKey($keyword){
    	//参数设定
    	$path = "/GetEnterpriseListByNameKey?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sNameKey={$keyword}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //返回企业详细信息
    public function GetEnterpriseInfoByCode($keyword){
        //参数设定
        $path = "/GetEnterpriseInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sEnterpriseCode={$keyword}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //获取业务知识库类别列表信息
    public function GetCKnowledge_CategoryList(){
        //参数设定
        $path = "/GetCKnowledge_CategoryList?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据查询关键字或知识库类别ID，返回业务知识库列表信息
    public function GetCKnowledge_List($keyword , $id){
        //参数设定
        $path = "/GetCKnowledge_List?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sSubjectKey={$keyword}&sKnowledge_Category_id={$id}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据业务知识库ID，返回业务知识库详细信息
    public function GetCKnowledgeInfo($id){
        //参数设定
        $path = "/GetCKnowledgeInfo?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sCKnowledge_ID={$id}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据审批事项名称关键字或申请单位关键字或申请编号，返回许可结果公示列表信息
    public function GetXZXK_List($sSPSXKey , $sSQDWKey , $sSQBH){
        //参数设定
        $path = "/GetXZXK_List?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sSPSXKey={$sSPSXKey}&sSQDWKey={$sSQDWKey}&sSQBH={$sSQBH}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据企业代码，返回许可经营的信息
    public function GetCBuItemInfoByCode($sEnterpriseCode){
        //参数设定
        $path = "/GetCBuItemInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sEnterpriseCode={$sEnterpriseCode}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据企业代码，返回股权质押的信息
    public function GetGQZYInfoByCode($sEnterpriseCode){
        //参数设定
        $path = "/GetGQZYInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sEnterpriseCode={$sEnterpriseCode}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据企业代码，返回动产抵押的信息
    public function GetDCDYInfoByCode($sEnterpriseCode){
        //参数设定
        $path = "/GetDCDYInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sEnterpriseCode={$sEnterpriseCode}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据企业代码，返回法院冻结的信息
    public function GetFYDJInfoByCode($sEnterpriseCode){
        //参数设定
        $path = "/GetFYDJInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sEnterpriseCode={$sEnterpriseCode}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据企业代码，查询变更历史
    public function GetModifyInfoByCode($input){
        //参数设定
        $path = "/GetModifyInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&input={$input}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }

    //根据企业代码，返回经营异常的信息
    public function GetYLMLInfoByCode($sEnterpriseCode){
        //参数设定
        $path = "/GetYLMLInfoByCode?signature={$this->signature}&timestamp={$this->timestamp}&nonce={$this->nonce}&sEnterpriseCode={$sEnterpriseCode}";
        $url = $this->service . $path;

        Vendor('Unirest.Unirest');
        $response = \Unirest::get($url);

        $xml = simplexml_load_string($response->body);
        $arr = (array)$xml;
        $xml = simplexml_load_string($arr[0]);
        $arr = (array)$xml;

        return $arr;
    }
}
?>