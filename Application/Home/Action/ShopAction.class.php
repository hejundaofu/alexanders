<?php
namespace Home\Action;
use Think\Action;
class ShopAction extends Action {
    public function index() {
    	$m = M('Company_product');
        $opt['type'] = 'item';
        $opt['brandId'] = 120;
        $opt['enable'] = 1;
    	$result = $m->where($opt)->select();
        $this->assign('products', $result);

        $opt['type'] = 'package';
        $package = $m->where($opt)->select();
        $this->assign('package', $package);

    	$userInfo = session('userInfo');

    	if($userInfo)
            $userId = $userInfo['id'];
    	else
            $userId = 0;

        $this->assign('userId', $userId);

        if(session('orderInfo')){
            $tmp = session('orderInfo');

            if($tmp['orderType'] == 'package'){
                session('orderInfo', null);
            }else{
                $tmp['userId'] = $userId;
                session('orderInfo', $tmp);
            }
        }

        $this->assign('shopInfo', $orderInfo = session('orderInfo'));
      	$this->display();
    }

    public function package(){
        $m = M('Company_product');
        $opt['type'] = 'package';
        $opt['brandId'] = 120;
        $opt['enable'] = 1;
        $package = $m->where($opt)->select();

        $this->assign('package', $package);
        $this->display();
    }

    public function newOrder(){
    	$orderInfo = session('orderInfo');

    	if(!$orderInfo)
    		$this->error('错误访问', __APP__);

        $hours = date('H');

        if($hours <= 14)
            $startDay = date("Y-m-d");
        else
            $startDay = date("Y-m-d",strtotime("+1 day"));

        $this->assign('city', $this->getCity());
        $this->assign('startDay', $startDay);
    	$this->assign('userAddress', $this->getUserAddress($orderInfo['userId']));
    	$this->assign('orderInfo', $orderInfo);
    	$this->display();
    }

    private function getCity(){
        $m = M('Company_city');

        return $m->select();
    }

    public function finishOrder(){
        $orderNo = session('orderId');

        $m = M('Company_order');
        $result = $m->find($orderNo);

        if($result){
            $this->assign('orderInfo', $result);
            if($result['payType'] == 'wechatpay'){
                if($result['sign'] == 0){
                    $this->display('payByWechat');
                }
                else{
                    $this->sendOrderMsg($result);
                    $this->display();
                }
            }
            else{
                $this->sendOrderMsg($result);
                $this->display();
            }
        }
    }

    public function getOrderPayHandle(){
        $orderNo = I('post.orderNo');
        
        $req['Code'] = -1;
        $req['Message'] = '未支付';
        if(S('pay_'.$orderNo) == 1){
            $req['Code'] = 0;
            $req['Message'] = '支付成功';
        }

        $this->ajaxReturn($req, 'json');
    }

    public function qrcode(){
        $orderNo = session('orderId');
        $m = M('Company_order');
        $result = $m->find($orderNo);

        if($result){
            if($result['payType'] == 'wechatpay'){
                if($result['sign'] == 0){
                    $companyInfo = $this->getCompanyInfo();
                    if($companyInfo){
                        $options = array(
                            'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
                            'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
                            'partnerid'=>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
                            'partnerkey'=>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
                            'paysignkey'=>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
                        );
                        $wechat = new \Org\Weixin\Wechat($options);

                        $url = $wechat->createNativeUrl('order_'.$result['orderNo']);

                        Vendor('phpQrcode.phpqrcode');

                        $errorCorrectionLevel="L";
                        $matrixPointSize="4";
                        \QRcode::png($url,false,$errorCorrectionLevel,$matrixPointSize);
                    }
                }
                else{

                }
            }
            else{
            }
        }
    }

    /*
        getTimeSectionHandle 获取指定日期可用的配送时间段
     */
    public function getTimeSectionHandle(){
        $date = I('post.date');
        $city = I('post.city');

        $m = M('Company_city');
        $opt['city'] = $city;
        $cityInfo = $m->where($opt)->find();

        if($cityInfo){
            if($date == date('Y-m-d') && $cityInfo['todayEnable'] == '0'){
                $req['Message'] = '当前所选时间段无法配送。';
                $req['Code'] = -1;
                $this->ajaxReturn($req);
            }

            if($date >= date('Y-m-d')){
                $startTime = $cityInfo['startTimeRand'];

                if($date == date('Y-m-d')){
                    if(date('H') < ($cityInfo['endTimeRand'] - $cityInfo['todayStartHours'])){
                        $startTime = date('H') + $cityInfo['todayStartHours'];

                        if($startTime < $cityInfo['startTimeRand'])
                            $startTime = $cityInfo['startTimeRand'];

                        if($startTime%2 == 1)
                            $startTime++;
                    }else{
                        $req['Message'] = '当前所选时间段无法配送。';
                        $req['Code'] = -1;
                        $this->ajaxReturn($req);
                    }
                }

                $req['Code'] = 0;

                $req['TimeSection'] = array();

                if($startTime<$cityInfo['endTimeRand']){
                    $req['TimeSection'][0] = '任意时段配送';
                }

                $endTime = $cityInfo['endTimeRand'];
                if($endTime > 19){
                    $endTime = 19;
                }

                for($i=$startTime; $i<$endTime; $i=$i+2){
                    $index = count($req['TimeSection']);

                    if($i != 18)
                        $req['TimeSection'][$index] = $i.':00 - '.($i+$cityInfo['timeHoursRand']).':00';
                    else
                        $req['TimeSection'][$index] = $i.'点以后';
                }
            }else{
                $req['Message'] = '当前所选时间段无法配送。';
                $req['Code'] = -1;
            }

            $this->ajaxReturn($req);
        }else{ 
            $req['Message'] = '当前所选时间段无法配送。';
            $req['Code'] = -1;
            $this->ajaxReturn($req);
        }
    }

    public function finishOrderHandle(){
        $orderInfo = I('post.');

        $req = $this->checkOrder($orderInfo);

        if($req['Code'] == 0){
            $code = $this->saveAddress($orderInfo);

            if($code > 0){
                $code = $this->saveOrder($orderInfo);

                if($code > 0){
                    session('orderId', $code);

                    session('orderInfo', null);

                    $req['Code'] = 0;
                    $req['Message'] = '成功！';
                }
                else{
                    $req['Code'] = -2;
                    $req['Message'] = '保存订单失败！';
                }
            }
            else{
                $req['Code'] = -1;
                $req['Message'] = '保存地址失败';
            }
        }

        $this->ajaxReturn($req, 'JSON');
    }

    public function getAddressHandle(){
        $id = I('post.id');

        $m = M('Company_user_address');
        $result = $m->find($id);

        $this->ajaxReturn($result, "json");
    }

    private function checkOrder($orderInfo){
        $result['Code'] = 0;
        $result['Message'] = '成功';

        if($orderInfo['address'] == ''){
            $result['Code'] = 10001;
            $result['Message'] = '您未填写地址信息';
        }else if($orderInfo['address']['address'] == ''){
            $result['Code'] = 10002;
            $result['Message'] = '地址信息为空';
        }else if($orderInfo['address']['name'] == ''){
            $result['Code'] = 10003;
            $result['Message'] = '收货人信息为空';
        }else if(!preg_match("/^1[34578]\d{9}$/", $orderInfo['address']['tel'])){
            $result['Code'] = 10004;
            $result['Message'] = '收货人手机信息输入有误';
        }else if($orderInfo['address']['city'] == ''){
            $result['Code'] = 10005;
            $result['Message'] = '配送城市未选择';
        }else if($orderInfo['address']['district'] == ''){
            $result['Code'] = 10006;
            $result['Message'] = '配送地级区域未选择';
        }else if($orderInfo['time'] == ''){
            $result['Code'] = 10007;
            $result['Message'] = '配送时间未选择';
        }else if($orderInfo['time']['date'] == ''){
            $result['Code'] = 10008;
            $result['Message'] = '配送日期未选择';
        }else if($orderInfo['time']['section'] == ''){
            $result['Code'] = 10009;
            $result['Message'] = '配送时间段未选择';
        }

        return $result;
    }

    private function sendOrderMsg($orderInfo){
        $userInfo = session('userInfo');

        $goods = unserialize($orderInfo['goods']);
        for($i=0; $i<count($goods); $i++){
            if($i>0)
                $goodsText .= ',';
            $goodsText .= $goods[$i]['name']. '*' . $goods[$i]['num'];
        }

        $address = unserialize(($orderInfo['address']));

        if($orderInfo['sign'] == 1)
            $title = "您好，你的订单【{$orderInfo['orderNo']}】已成功支付，我们将尽快处理。";
        else
            $title = "您好，你的订单【{$orderInfo['orderNo']}】已成功提交，我们将尽快处理。";

        if($userInfo){
            $companyInfo = $this->getCompanyInfo();

            $options = array(
                'appid'      =>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
                'appsecret'  =>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
                'partnerid'  =>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
                'partnerkey' =>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
                'paysignkey' =>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
            );

            $data = array(
                'touser' => $userInfo['openId'],
                'template_id' => 'ffjlR69y7HeuQj1JsuGbZGDfNCOACC9q1LglPHLfduc',
                'url' => '',
                'topcolor' => '#000000',
                'data' => array(
                    'first' => array(
                        'value' => $title,
                        "color"=>"#173177"
                        ),
                    'keyword1' => array(
                        'value' => date('Y-m-d H:i:s'),
                        "color"=>"#173177"
                        ),
                    'keyword2' => array(
                        'value' => $orderInfo['payPrice'].'元',
                        "color"=>"#173177"
                        ),
                    'keyword3' => array(
                        'value' => $goodsText,
                        "color"=>"#173177"
                        ),
                    'keyword4' => array(
                        'value' => $address['tel'],
                        "color"=>"#173177"
                        ),
                    'keyword5' => array(
                        'value' => $address['address'],
                        "color"=>"#173177"
                        ),
                    'remark' => array(
                        'value' => "如有疑问，请拨打客服电话4009908355或添加微信客服icalexsz”",
                        "color"=>"#173177"
                        )
                    )
                );
            $wechat = new \Org\Weixin\Wechat($options);
            $wechat->sendTemplateMessage($data);
        }
    }

    private function getCompanyInfo(){
        $m = M('Company_info');

        return $m->find(888);
    }

    private function saveAddress($orderInfo){
        if($orderInfo['address']['id'])
            return $orderInfo['address']['id'];
        
        $opt = $orderInfo['address'];
        $opt['uId'] = $orderInfo['userId'];

        $m = M('Company_user_address');
        $result = $m->add($opt);

        return $result; 
    }

    private function saveOrder($orderInfo){
        $m = M('Company_order');

        $opt['orderNo'] = $this->getOrderNo();
        $opt['uId'] = $orderInfo['userId'];
        $opt['address'] = serialize($orderInfo['address']);
        $opt['goods'] = serialize($orderInfo['items']);
        $opt['price'] = $orderInfo['prices'];
        $opt['payPrice'] = $orderInfo['payPrice'];
        $opt['discount'] = $orderInfo['discount'];
        $opt['goodsTotal'] = $orderInfo['count'];
        $opt['payType'] = $orderInfo['payment'];
        $opt['sendDay'] = $orderInfo['time']['date'];
        $opt['sendTime'] = $orderInfo['time']['section'];
        $opt['addTime'] = date('Y-m-d H:i:s');
        $opt['sign'] = 0;
        $opt['Message'] = $orderInfo['msg'];
        $opt['type'] = $orderInfo['type'];
        $opt['city'] = $orderInfo['address']['city'];

        if($opt['type']){
            $opt['ownerName'] = $orderInfo['OwnerName'];
            $opt['ownerTel'] = $orderInfo['OwnTel'];
        }

        $result = $m->add($opt);

        return $result;
    }

    private function getOrderNo(){
        $m = M('Company_order');

        $result = $m->count();

        $id = $result + 1;

        $pre = sprintf('%02d', $id / 14000000);// 每1400万的前缀
        // 这里乘以 123456789 一是一看就知道是9位长度，二则是产生的数字比较乱便于隐蔽
        $tempcode = sprintf('%09d', sin(($id % 14000000 + 1) / 10000000.0) * 123456789);   
        $seq = '371482506';// 这里定义 0-8 九个数字用于打乱得到的code
        $code = '';
        for ($i = 0; $i < 9; $i++)
        {
            $code .= $tempcode[ $seq[$i] ];
        }
        return '8'.$pre.$code;
    }
    private function getUserAddress($uId){
        $m = M('Company_user_address');

        $opt['uId'] = $uId;

        $result = $m->where($opt)->select();

        return $result;
    }
}
