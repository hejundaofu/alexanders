<?php
namespace Home\Action;
use Think\Action;
use Org\Net\Http;
class ExchangeAction extends Action {
    public function payQrcode(){
      $openid = I('get.openid');
      $id = I('get.id');

      $m = M('Company_pay_qrcode_paylog');

      $result = $m->find($id);

      if($result && !$result['exchangeSign']){
        $url = $this->getRedirectUrl('exchangeQrcode', $openid, $id);

        Vendor('phpQrcode.phpqrcode');

        $errorCorrectionLevel="L";
        $matrixPointSize="4";
        \QRcode::png($url,false,$errorCorrectionLevel,$matrixPointSize);
      }else{
        echo '您已领取！';
      }
    }

    private function getRedirectUrl($type, $openid, $id){
        $url = \Weixin\Response\WechatConst::REDIRECT_DOMAIN;

        $url .="{$type}&user={$openid}&id={$id}";

        $oauthUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=<-appid->&redirect_uri=<-url->&response_type=code&scope=snsapi_base&state=1#wechat_redirect";

        $url = urlencode($url);

        $oauthUrl = str_replace("<-url->", $url, $oauthUrl);

        $companyInfo = $this->getAppToken(888);

        $oauthUrl = str_replace("<-appid->", $companyInfo['weixin_AppId'], $oauthUrl);

        return $oauthUrl;
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }
}
