<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Action;
use Think\Action;
class LoginAction extends Action{
    public function index(){
        session('user_name' , NULL);
       
        $this->display("newIndex");
    }
    
    public function doLogin(){
        
       
        //验证验证码是否正确
        if(!$this->check_verify($_POST['verify'])) {
            $this->error("验证码错误");
        }
        
        //检查数据库是否存在用户
        $m = M("User_info");
        
        $condition['user_name'] = $_POST['user_name'];
        $condition['user_password'] = $_POST['user_password'];
        
        $result = $m->where($condition)->find();
        
        if($result != '')
        {
            session('user_permission' , $result["user_permission"]);
            session('user_name', $_POST['user_name']);
            session('ecid' , $result["company_ecid"]);
            redirect(__ROOT__.'/admin.php');
        }
        else
            $this->error ("用户名或密码错误！");

    }

    private function check_verify($code, $id = ''){
        $verify = new \Think\Verify();
        return $verify->check($code, $id);

    }
 
}
?>