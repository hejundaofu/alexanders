<?php
namespace Home\Action;
use Think\Action;
class UserAction extends Action {
	public function index(){
		$userInfo = session('userInfo');
    	if($userInfo)
        	$userId = $userInfo['id'];
    	else
        	$userId = 0;

    	$this->assign('userInfo', $userInfo);
		$this->display();
	}

	public function order(){
		$userInfo = session('userInfo');
    	if($userInfo)
        	$userId = $userInfo['id'];
    	else
        	$userId = 0;

    	$this->assign('userInfo', $userInfo);

    	$Data = M('Company_order');

		$params = I('post.');

		$opt['uId'] = $userId;

		$count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $order = $Data->where($opt)->order( 'addTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        if($order){
        	for($i=0; $i<count($order); $i++){
        		$order[$i]['payType'] = $this->getPayType($order[$i]['payType']);

        		$order[$i]['paySign'] = $this->getPaySign($order[$i]['payTime']);

        		$order[$i]['wlSign'] = $this->getWlSign($order[$i]['sign']);

        		$order[$i]['userInfo'] = unserialize($order[$i]['address']);
        		$order[$i]['goods'] = unserialize($order[$i]['goods']);
        	}
        }

        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign('order', $order);
        $this->assign('params', $opt);
		$this->display();
	}

	private function getPayType($payCode){
		$payment = '';
		switch ($payCode) {
			case 'wechatpay':
				$payment = '微信支付';
				break;
			
			default:
				$payment = '货到付款';
				break;
		}

		return $payment;
	}

	private function getWlSign($sign){
		$wlSign = '';
		
		switch($sign){
			case 1:
				$wlSign = '待配送';
				break;
			case 2:
				$wlSign = '配送中';
				break;
			case 3:
				$wlSign = '已完成';
				break;
			default:
				$wlSign = '未完成订单';
				break;
		}

		return $wlSign;
	}

	private function getPaySign($payTime){
		$paySign = '';
		
		if($payTime != '')
			$paySign = "<span class='text-success'>已支付</span>";
		else
			$paySign = "<span class='text-danger'>未支付</span>";

		return $paySign;
	}
}