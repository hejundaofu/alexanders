<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Action;
use Think\Action;
class PublicAction extends Action{
    public function verify(){
    	$config =    array(
			    'fontSize'    =>    15,    // 验证码字体大小
			    'length'      =>    4,     // 验证码位数
			    'useNoise'    =>    false, // 关闭验证码杂点
			);

        $Verify = new \Think\Verify($config);
		$Verify->entry();
    }

    public function sendCustomMsg(){
    	$companyInfo = $this->getCompanyInfo();

    	$options = array(
			'appid'      =>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
			'appsecret'  =>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
			'partnerid'  =>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
			'partnerkey' =>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
			'paysignkey' =>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
        );

        $data = array(
        	'touser' => 'ooKmYt8VGB2V3l3ZXolpFbTukE58',
        	'template_id' => '9vrZ54E2usz08SVYjManqdUlHlI--x3eMqWdXKBMwpI',
        	'url' => 'http://www.icalex.com',
        	'topcolor' => '#000000',
        	'data' => array(
        		'first' => array(
        			'value' => "您好，你的订单已成功提交",
        			"color"=>"#173177"
        			),
        		'keyword1' => array(
        			'value' => "2015-01-06 12：12：11",
        			"color"=>"#173177"
        			),
        		'keyword2' => array(
        			'value' => '158元',
        			"color"=>"#173177"
        			),
        		'keyword3' => array(
        			'value' => '香草口味*8',
        			"color"=>"#173177"
        			),
        		'keyword4' => array(
        			'value' => 'XX街XX小区XX楼XX室',
        			"color"=>"#173177"
        			),
        		'remark' => array(
        			'value' => "你的订单已成功支付，如有疑问，请拨打客服电话！",
        			"color"=>"#173177"
        			)
        		)
        	);
        $wechat = new \Org\Weixin\Wechat($options);

        dump($wechat->sendTemplateMessage($data));
    }

    private function getCompanyInfo(){
    	$m = M('Company_info');

        return $m->find(888);
    }
}
?>