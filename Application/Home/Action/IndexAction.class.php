<?php
namespace Home\Action;
use Think\Action;
use Org\Net\Http;
class IndexAction extends Action {
    public function index() {
      $uachar = "/(nokia|sony|ericsson|mot|samsung|sgh|lg|philips|panasonic|alcatel|lenovo|cldc|midp|mobile)/i";

      if($this->is_mobile()){
        $this->redirect('Mobile/Index/index?showwxpaytitle=1');
      }

      $userInfo = session('userInfo');
      if($userInfo)
          $userId = $userInfo['id'];
      else
          $userId = 0;

      $this->assign('userInfo', $userInfo);
      $this->display();
    }

    public function admin(){
      $this->display('new_admin');
    }

    public function doLoginHandle(){
      $data = I('post.');

      $req['code'] = -1;
      $req['info'] = '一般错误！';

      if($data){
        $m = M('Company_888_user_info');
        $result = $m->find($data['user']['id']);

        if($result){
          session('userInfo', $result);

          $req['code'] = 0;
          $req['info'] = '正确！';
          
        } 
      }

      $this->ajaxReturn($req, 'JSON');
    }

    public function login(){
      $scene = time();

      $qrImg = $this->getQrPicUrl(888, $scene);

      $this->assign('scene', $scene);
      $this->assign('qrImg', $qrImg);
      $this->display();
    }

    public function getLoginInfo(){
      $this->assign('userInfo', session('userInfo'));
      $this->display();
    }

    public function getLoginModel(){
      $scene = time();

      $qrImg = $this->getQrPicUrl(888, $scene);

      $this->assign('scene', $scene);
      $this->assign('qrImg', $qrImg);

      $this->display();
    }

    public function doQuit(){
      session(null);
      $this->success('退出成功！', __APP__ );
    }

    private function getQrPicUrl($ecid , $sceneId){
      $m = M("Company_info");

      $opt['company_ecid'] = $ecid;

      $token = $m->where($opt)->find();

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            return;
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
          $qrCode = $weObj->getQRCode($sceneId, 0);
            if($qrCode){
                return $weObj->getQRUrl($qrCode['ticket']);
            }
        }
    }

    private function is_mobile() { 
        $user_agent = $_SERVER['HTTP_USER_AGENT']; 
        $mobile_agents = array("240x320","acer","acoon","acs-","abacho","ahong","airness","alcatel", 
     "amoi","android","anywhereyougo.com","applewebkit/525","applewebkit/532","asus","audio", 
        "au-mic","avantogo","becker","benq","bilbo","bird","blackberry","blazer","bleu", 
        "cdm-","compal","coolpad","danger","dbtel","dopod","elaine","eric","etouch","fly ", 
        "fly_","fly-","go.web","goodaccess","gradiente","grundig","haier","hedy","hitachi", 
        "htc","huawei","hutchison","inno","ipad","ipaq","iphone","ipod","jbrowser","kddi", 
        "kgt","kwc","lenovo","lg ","lg2","lg3","lg4","lg5","lg7","lg8","lg9", 
     "lg-","lge-","lge9","longcos","maemo", 
        "mercator","meridian","micromax","midp","mini","mitsu","mmm","mmp","mobi","mot-", 
        "moto","nec-","netfront","newgen","nexian","nf-browser","nintendo","nitro","nokia", 
        "nook","novarra","obigo","palm","panasonic","pantech","philips","phone","pg-", 
        "playstation","pocket","pt-","qc-","qtek","rover","sagem","sama","samu","sanyo", 
        "samsung","sch-","scooter","sec-","sendo","sgh-","sharp","siemens","sie-","softbank", 
        "sony","spice","sprint","spv","symbian","tablet","talkabout","tcl-","teleca","telit", 
        "tianyu","tim-","toshiba","tsm","up.browser","utec","utstar","verykool","virgin", 
        "vk-","voda","voxtel","vx","wap","wellco","wig browser","wii","windows ce", 
        "wireless","xda","xde","zte"); 
        $is_mobile = false; 
        foreach ($mobile_agents as $device) { 
            if (stristr($user_agent, $device)) { 
                $is_mobile = true; 
                break; 
            } 
        } 
        return $is_mobile; 
    }

    public function activityNews(){
        $opt['id'] = $_GET['id'];
        $m = M('View_company_activity');
        $result = $m->where($opt)->find();
        $result['content'] = htmlspecialchars_decode($result['content']);
        $companyInfo = M("Company_info")->where("company_ecid = ".$result['ecid'])->find();
        $this->assign('company' , $companyInfo);
        $this->assign('activity',$result);
        $this->display();
    }

    public function doLogin() {
        //验证验证码是否正确

        // if (!$this->checkVerify($_POST['verify'])) {
        //     $this->error ( \Org\Error\Error::getErrMsg( \Org\Error\Error::ERROR_WRONG_VERIFY ) );
        // }

        //检查数据库是否存在用户
        $code = $this->checkUser();

        if ( $code != \Org\Error\Error::SUCCESS_OK ) {
            $this->error( \Org\Error\Error::getErrMsg( $code ) );
        }

        redirect( U('Admin/Index/index') );
    } 

    public function news() {
        $data = M( "Company_news" )->where( "id = " . $_GET["id"] )->find();
          $data['content'] = htmlspecialchars_decode($data['content']);
  
        $params = I('get.');
        $this->saveRead($params);

        $this->assign( 'newsArr' , $data );
        //$this->assign( 'params', $session);
        $this->display();
       
        // if(S('news_view_'.$params['r'])){
        //   $this->assign( 'newsArr' , $data );
        //   $this->assign( 'params', $session);
        //   $this->display();
        // }else{
        //   $url = \Weixin\Response\WechatConst::REDIRECT_DOMAIN . "ecid={$params['ecid']}&type=news&fromOpenId={$params['openid']}&from_type=1&newsId={$params['id']}";

        //   $base_url = $this->getOauthUrl($url, $params['ecid']);

        //   header("Location: {$base_url}");
        // }
    }

    private function saveRead($params){
      if($params['from'] == 'timeline'){
        $m = M('News_view_log');

        $opt['fromOpenId'] = $params['openid'];
        $opt['type'] = 2;
        $opt['ecid'] = $params['ecid'];
        $opt['newsId'] = $params['id'];
        $opt['time'] = date('Y-m-d H:i:s');

        $m->add($opt);
      }
    }

    /**
     * 检查用户名、密码是否正确
     * @return [int] [错误码]
     */
    private function checkUser() {
        //检查数据库是否存在用户
        $ecid = I('post.ecid',0);
        $user = I('post.user_name','-1');
        $password = md5(I('post.user_password'));

        if(substr($user,0,1) == '@'){
          $ecid = 816;
          $user = substr($user, 1);
        }
        $m = M( "User_info" );
        $condition['company_ecid'] = $ecid;
        $condition['user_name'] = $user;
        $condition['user_password'] = $password;
        $result = $m->where( $condition )->find();

        //用户正确时记录用户信息
        if ( $result != '' ) {
            $userCfg = C( 'LOGIN_USER' );

            session( $userCfg['NAME'], $user );
            session( $userCfg['ECID'] , I('post.ecid') );
            session( $userCfg['UID'] , $result["id"] );
            session( $userCfg['ROLE'] , $result["roleId"]);
            session( $userCfg['CITY'], $result['city']);

            if($result['roleId'] == 'dealer'){
              session( $userCfg['DEALERID'] , $result["dealerId"] );
            }

            return \Org\Error\Error::SUCCESS_OK;
        }
        else {
            return \Org\Error\Error::ERROR_WRONG_LOGIN_INFO;
        }
    }

    /**
     * 检查验证码是否正确
     * @return [int] [错误码]
     */
    private function checkVerify($code, $id='') {
      $verify = new \Think\Verify();
      return $verify->check($code, $id);
    }


}
