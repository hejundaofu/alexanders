<?php
namespace Home\Action;
use Think\Action;
use Org\Net\Http;
class CompanyAction extends Action {
	public function eat() {
		$this->display();
	}

	public function result(){
		$m = M('Company_try_eat');

		if (!$m->autoCheckToken($_POST)){
		 // 令牌验证错误
		 	$this->assign('info', '表单已过期。');
		 }else{
		 	$opt = I('post.');

		 	unset($opt['__hash__']);

		 	$opt['addTime'] = date('Y-m-d');

		 	$result = $m->add($opt);

		 	if($result){
		 		$this->assign('info', '申请成功');
		 		$this->assign('subInfo', "ALEXANDER'S（亚历山达）会对申请试吃的企业进行筛选，如果您的申请通过，我们会在7个工作日内与您联系。");
		 	}
		 	else
		 		$this->assign('info', '申请失败');
		 }
		$this->display();
	}
}