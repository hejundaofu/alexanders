<?php
namespace Home\Action;
use Think\Action;
class ProductAction extends Action {
    public function item() {
    	$id = I('get.id');

    	$m = M('Company_product');
        $opt['brandId'] = 120;
        $opt['enable'] = 1;
    	$result = $m->where($opt)->select();

    	if($id)
    		$this->assign('productInfo', $m->find($id));
    	else{
    		$this->assign('productInfo', $result[0]);
    	}

        $userInfo = session('userInfo');
        if($userInfo)
            $userId = $userInfo['id'];
        else
            $userId = 0;

        $orderInfo = session('orderInfo');
        if($orderInfo)
            $this->assign("orderInfo", $orderInfo);
        else{
            $orderInfo['userId'] = $userId;
            $orderInfo['count'] = 0;
            $orderInfo['prices'] = 0;
            $orderInfo['discount'] = 0;
            $orderInfo['payPrice'] = 0;
            $orderInfo['items'] = array();
            $this->assign('orderInfo', $orderInfo);
        }

        $this->assign('userInfo', $userInfo);
        $this->assign('userId', $userId);
    	$this->assign('products', $result);
    	$this->display('item-new');
    }

    public function index(){
        $m = M('Company_product');
        $opt['brandId'] = 120;
        $opt['enable'] = 1;
        $result = $m->where($opt)->select();

        $userInfo = session('userInfo');
        if($userInfo)
            $userId = $userInfo['id'];
        else
            $userId = 0;

        $this->assign('userInfo', $userInfo);

        $orderInfo = session('orderInfo');
        if($orderInfo)
            $this->assign("orderInfo", $orderInfo);
        else{
            $orderInfo['userId'] = $userId;
            $orderInfo['count'] = 0;
            $orderInfo['prices'] = 0;
            $orderInfo['discount'] = 0;
            $orderInfo['payPrice'] = 0;
            $orderInfo['items'] = array();
            $this->assign('orderInfo', $orderInfo);
        }

        $this->assign('userId', $userId);
        $this->assign('products', $result);
        $this->display('index-new');
    }

    public function quickBuyHandle(){
        $id = I('post.id');
        $userInfo = session('userInfo');

        if($id && $userInfo){
            $m = M('Company_product');
            $result = $m->find($id);
            if($result){
                if($result['type'] == 'item'){
                    $orderInfo['count'] = 8;
                    $orderInfo['orderType'] = 'item';
                    $orderInfo['discount'] = 0;
                    $orderInfo['userId'] = $userInfo['id'];
                    $orderInfo['prices'] = 8*$result['price'];
                    $orderInfo['payPrice'] = $orderInfo['prices'] -  $orderInfo['discount'];
                    $orderInfo['items'] = array($result);
                    $orderInfo['items'][0]['num'] = 8;
                }else{
                    $orderInfo['count'] = 1;
                    $orderInfo['orderType'] = 'package';
                    $orderInfo['discount'] = 0;
                    $orderInfo['userId'] = $userInfo['id'];
                    $orderInfo['prices'] = $result['price'];
                    $orderInfo['payPrice'] = $orderInfo['prices'] -  $orderInfo['discount'];
                    $orderInfo['items'] = array($result);
                    $orderInfo['items'][0]['num'] = 1;
                }

                session('orderInfo',$orderInfo);
                $req['Code'] = 0;
                $req['Message'] = 'success';
                $this->ajaxReturn($req, 'JSON');
            }
        }

        $req['Code'] = -1;
        $req['Message'] = 'error';
        $this->ajaxReturn($req, 'JSON');
    }
}
