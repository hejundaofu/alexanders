<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Admin\Action;
use Org\Error\Error;
class MenuAction extends AdminAction {
    public function index() {
        $this->assign('appToken',$this->getAppToken(session('ecid')));
        $this->display();
    }

    public function refreshMenu(){
        $this->assign('menuArr',$this->getMenuSet(session('ecid')));
        $this->display();
    }

    public function showEditHtml(){
        $opt = I('post.id');

        $m = M('Company_menu');
        $result = $m->find($opt);
        $result['informKeyword'] = explode(",",$result['informKeyword']);

        $this->assign('menu',$result);
        $this->assign('menuId',$opt);
        $this->display();
    }

    public function editTokenHandle(){
        $opt = I('post.');

        $m = M("Company_info");

        $code = $m->save($opt);

        $this->ajaxReturn($code,'info',0);
    }

    public function delMenuHandle(){
        
        $opt = I('post.');

        $m = M('Company_menu');
        if($opt['id'] != ''){
            if($opt['hasSub'] == 1){
                $m->where('rootId = '. $opt['id'])->delete();
            }

            $result = $m->where('id = '.$opt['id'])->find();
            if($m->where('rootId = ' . $result['rootId'] . ' AND id != ' . $result['id'])->select()){

            }else{
                $arr = array(
                    "id" => $result['rootId'] , 
                    "hasSub" => 0
                    );

                $m->save($arr);
            }
            if($m->where('id = '. $opt['id'])->delete()){
                $result["data"] = Error::SUCCESS_OK;
            }else{
                $result["data"] = Error::ERROR_EDIT_HANDLE_ERR;
                $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }

            $this->ajaxReturn($result,"JSON");
        }
    }

    public function addMenuHandle(){
        
        $opt = I('post.');
        $opt['ecid'] = session('ecid');

        $m = M('Company_menu');
        if($opt['rootId'] == -1){
            //一级菜单不得超过4个字符
            if(mb_strlen( $opt['name'], "UTF8" ) > 4)
                $data["data"] = Error::ERROR_MENU_ROOT_LONG;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_ROOT_LONG);
           

            $subOpt['rootId'] = -1;
            $subOpt['ecid'] = session('ecid');

            $count = $m->where($subOpt)->count();
            if($count >= 3){
                $data["data"] = Error::ERROR_MENU_ROOT_COUNT;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_ROOT_COUNT);

                $this->ajaxReturn($data,"JSON");
            }  
        }
        else{
            //子菜单不得超过7个字符
            if(mb_strlen( $opt['name'], "UTF8" ) > 7)
                $data["data"] = Error::ERROR_MENU_SUB_LONG;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_SUB_LONG);
                

            $subOpt['rootId'] = $opt['rootId'];
            $subOpt['ecid'] = session('ecid');

            $count = $m->where($subOpt)->count();

            if($count >= 5)
                $data["data"] = Error::ERROR_MENU_SUB_COUNT;
                $data["info"] = Error::getErrMsg(Error::ERROR_MENU_SUB_COUNT);
                
        }
        
        $result=$m->where("ecid = ".$opt['ecid']. " AND name = '".$opt['name']."'" )->find();
        
        if(!$result){

        $time = time();
        $opt['ecid'] = session('ecid');
        $opt['key'] = 'SZ12365_MENUCLICK_' . $time;
        $opt['sort'] = $time;
        
        if($m->add($opt)){
                    $data["data"] = Error::SUCCESS_OK;
                }else{
                    $data["data"] = Error::ERROR_EDIT_HANDLE_ERR;
                    $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                }
        }else{
            $data["data"] = Error::ERROR_MENU_NAME_EXIST;
            $data["info"] = Error::getErrMsg(Error::ERROR_MENU_NAME_EXIST);
        }
        if($opt['rootId'] != -1)
            $this->setSubMenuSign($opt['rootId']);

        $this->ajaxReturn($data,"JSON");
    }

    public function editMenuHandle(){

        $opt = I('post.');
        unset($opt['activityName']);

        $opt['informKeyword'] = ltrim(implode(",", I('post.informKeyword')),',');

        $m = M('Company_menu');
        $option['name']=I('post.name');
        $result=$m->where("$option AND id != ".I('post.id'))->find();
        if(!$result){            
            $opt['modifyUserId'] = session($this->_userCfg['UID']);
            $opt['modifyTime'] = date("Y-m-d H:i:s");
            if($m->save($opt)){
                $menu_cache = 'weixin_menu'. $opt['ecid'].$opt['key'];
                S($menu_cache, NULL);
                $data['data'] = Error::SUCCESS_OK;
                $data["info"] = Error::getErrMsg(Error::SUCCESS_OK);
            }
            else{
                $data['data'] = Error::ERROR_EDIT_HANDLE_ERR;
                $data["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
            }
        }
        else{
            $data['data'] = Error::ERROR_MENU_NAME_EXIST;
            $data["info"] = Error::getErrMsg(Error::ERROR_MENU_NAME_EXIST);
        }

        $this->ajaxReturn($data,"JSON");
    }

    public function releaseMenuHandle(){
        $token = $this->getAppToken(session('ecid'));

        //如果接口信息不完整，直接返回
        if($token['weixin_AppId']=='' || $token['weixin_AppSecret']==''){
            $this->ajaxReturn(Error::ERROR_MENU_TOKEN_EMPTY,Error::getErrMsg(Error::ERROR_MENU_TOKEN_EMPTY),0);
        }

        $menu = $this->getMenuSet(session('ecid'));
        $menu_weixin = array('button' => '' );

        for($i=0; $i<count($menu); $i++){
            $menu_weixin['button'][$i] = $this->changeWeixinArray($menu[$i] , $token);

            if($menu[$i]['hasSub'] == 1){
                for($j = 0;$j<count($menu[$i]['subitem']);$j++){
                    if(($menu[$i]['subitem'][$j]['responseType'] == null || $menu[$i]['subitem'][$j]['responseType'] == "text") && $menu[$i]['subitem'][$j]['responseText'] == ""){
                        $data['data'] = ERROR::ERROR_MENU_REPLY_EMPTY;
                        $data['info'] = $menu[$i]['subitem'][$j]['name'] . "：" . Error::getErrMsg(Error::ERROR_MENU_REPLY_EMPTY);
                        $this->ajaxReturn($data , "JSON");
                    }
                }
            }else{
                if(($menu[$i]['responseType'] == "text" || $menu[$i]['responseType'] == null) && $menu[$i]['responseText'] == ""){
                    $data['data'] = ERROR::ERROR_MENU_REPLY_EMPTY;
                    $data['info'] = $menu[$i]['name'] . "：" . Error::getErrMsg(Error::ERROR_MENU_REPLY_EMPTY);
                    $this->ajaxReturn($data , "JSON");
                }
            }
            
        }

        $weObj = new \Org\Weixin\Wechat();

        if($weObj->checkAuth($token['weixin_AppId'], $token['weixin_AppSecret'])){
            if($tmp = $weObj->createMenu($menu_weixin)){
                $data['data'] = Error::SUCCESS_OK;
                $data['info'] = Error::getErrMsg(Error::SUCCESS_OK);
                $this->ajaxReturn($data,'JSON');
            }
        }
    }

    private function changeWeixinArray($menuItem , $token){
        $item = '';
        $item['name'] = $menuItem['name'];
        if($menuItem['hasSub'] == 1){
            $subItem = $menuItem['subitem'];

            for($i=0; $i<count($subItem); $i++){
                $item['sub_button'][$i] = $this->changeWeixinArray($subItem[$i] , $token);
            }
        }
        else{
            switch ($menuItem['responseType']) {
                case 'text':
                case 'news':
                case 'check_prize':
                case 'information':
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'view':
                    $item['type'] = 'view';
                    $item['url'] = $menuItem['linkURL'];
                    break;
                case 'service':
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'mall':
                    $url = $this->getRedirectUrl($menuItem['mall_keyword'], 888);
                    if($url){
                        $item['type'] = 'view';
                        $item['url'] = $url;
                    }
                    else{
                        $item['type'] = 'click';
                        $item['key'] = $menuItem['key'];
                    }
                    break;
                case 'vip':
                    $url = $this->getRedirectUrl($menuItem['vip_keyword'], session( "ecid" ));
                    if($url){
                        $item['type'] = 'view';
                        $item['url'] = $url;
                    }
                    else{
                        $item['type'] = 'click';
                        $item['key'] = $menuItem['key'];
                    }
                    break;
                case 'activity':
                    // $item['type'] = 'view';
                    // $item['url'] = $this->getActivityUrl($menuItem['activityId'] , $token['weixin_AppId']);
                    $item['type'] = 'click';
                    $item['key'] = $menuItem['key'];
                    break;
                case 'qrcode':
                    $item['type'] = 'scancode_waitmsg';
                    $item['key'] = $menuItem['key'];
                    break;
                default:
                    break;
            }
        }

        return $item;
    }

    private function getRedirectUrl($type, $ecid){
        $url = \Weixin\Response\WechatConst::REDIRECT_DOMAIN;

        $url .= $type;

        $oauthUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=<-appid->&redirect_uri=<-url->&response_type=code&scope=snsapi_base&state=1#wechat_redirect";

        $url = urlencode($url);

        $oauthUrl = str_replace("<-url->", $url, $oauthUrl);

        $companyInfo = $this->getAppToken($ecid);

        $oauthUrl = str_replace("<-appid->", $companyInfo['weixin_AppId'], $oauthUrl);

        return $oauthUrl;
    }

    /**
    * 定义函数
    * getActivityUrl
    * 函数功能描述
    * 获取活动相关的菜单链接
    * @access private
    * @param int $activityId 活动id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-06 创建函数
    */
    private function getActivityUrl($activityId , $weixin_AppId){
        $m = M('Company_activity');
        $opt['id'] = $activityId;
        $result = $m->where($opt)->find();

        $url = '';

        switch ($result['type']) {
            case 'question':
                $data['ecid'] = $result['ecid'];
                $data['activityId'] = $activityId;
                $data['type'] = $result['type'];

                $url = $this->getQuestionRedirectUrl($data , $weixin_AppId);
                break;
            
            default:
                # code...
                break;
        }

        return $url;
    }

    /**
    * 定义函数
    * getQuestionRedirectUrl
    * 函数功能描述
    * 获取问题活动的链接
    * @access private
    * @param array $data
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-06 创建函数
    */
    private function getQuestionRedirectUrl($data , $weixin_AppId){
        $url = \Weixin\Response\WechatConst::SERVER_DOMAIN."/Api/Redirect?";
        $params = http_build_query($data);
        $url .= $params;

        $response = new \Weixin\Response\Response();
        return $response->getOauthRedirectUrl($url , $weixin_AppId);
    }

    private function setSubMenuSign($rootIdID){
        $m = M('Company_menu');
        $opt['id'] = $rootIdID;
        $opt['hasSub'] = 1;
        $m->save($opt);
    }

    private function getMenuSet($ecid){
        $m = M("Company_menu");

        $opt['ecid'] = $ecid;
        $opt['rootId'] = -1;

        $result = $m->where($opt)->order('sort asc')->select();

        for($i=0; $i<count($result); $i++){
            if($result[$i]['hasSub'] == 1){
                $subOpt['rootId'] = $result[$i]['id'];

                $result[$i]['subitem'] = $m->where($subOpt)->order('sort asc')->select();
            }
        }

        return $result;
    }

    private function getAppToken($ecid){
        $m = M("Company_info");

        $opt['company_ecid'] = $ecid;

        $result = $m->where($opt)->find();

        return $result;
    }

    public function change() {
        $this->assign( 'userName', session( 'user_name' ) );//用户名

        import( '@.WechatMenu.MenuDbControl' );
        $menuControl = new MenuDbControl( session( "ecid" ) );
        $result = $menuControl->getWeixinMenuArray();
        $material = M( "Company_".session( "ecid" )."_material_group" );
        $materialCount = $material->count();
        $materialPage = ceil( $materialCount/4 );

        $this->setToken();
        $this->assign( 'materialCount', $materialCount );// 素材总数
        $this->assign( 'materialPage', $materialPage );// 素材页数
        $this->assign( 'result', array( "button"=>$result ) );
        $this->display();
    }
}
?>
