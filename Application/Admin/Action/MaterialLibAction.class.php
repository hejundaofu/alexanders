<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Admin\Action;
use Org\Error\Error;
class MaterialLibAction extends AdminAction{
    public function index() {
        //分页
        // 导入分页类
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $Data = M( "Company_material_group" );
        $count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 10 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $groupResult = $Data->where($opt)->order( 'id desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $newsArr = null;     //记录数据库数据
        if($groupResult){
            for ( $i = 0;$i<count( $groupResult );$i++ ) {
                $newsArr[$i]["groupId"] = $groupResult[$i]['id'];
                $newsArr[$i]["modifyTime"] = $groupResult[$i]['modifyTime'];
                $newsId = explode( ",", $groupResult[$i]["materialId"] );

                $newsRow = null;
                for ( $j = 0;$j<count( $newsId );$j++ ) {
                    $newsResult = M( "Company_news" )->where( 'id = ' . $newsId[$j] )->find();

                    $newsRow[$j] = array(
                        'title' => $newsResult['title'],
                        'description' => $newsResult['description'],
                        'content' => $newsResult['content'],
                        'newsImg' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg'],
                        'synWeb' => $newsResult['synWeb']
                    );
                }
                if(count($newsRow) > 1)
                    $newsArr[$i]['multiple'] = 1;
                else
                    $newsArr[$i]['multiple'] = 0;
                $newsArr[$i]['Result'] = $newsRow;
            }
        }

        $this->assign( 'page', $show );// 赋值分页输出
        $this->setToken();
        $this->assign( "newsArr" , $newsArr );//赋值素材数据
        $this->display('new-index');

    }

    public function AddNews() {
        $this->setToken();
        $this->display();
    }

    private function AddNewsGroup( $sort='' ) {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        $data = array(
            'ecid' => session($this->_userCfg['ECID']) , 
            'modifyTime' => date( "Y-m-d H:i:s" ),
            'modifyUserId' => session($this->_userCfg['UID'])
        );

        return M( "Company_material_group" )->data( $data )->add();
    }

    public function AddNewsHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        if ( I('post.items') != '' ) {
            $items = I('post.items');

            for ( $i = 0;$i<count( $items );$i++ ) {
                $opt['ecid'] = session($this->_userCfg['ECID']);
                $opt['title'] = $items[$i]['title'];
                if($this->isDataExist('Company_news' , $opt)){
                    $result['status'] = Error::ERROR_NEWS_NAME_EXIST;
                    $result['info'] = Error::getErrMsg(Error::ERROR_NEWS_NAME_EXIST);
                    $this->ajaxReturn($result , "JSON");
                }
            }
            
            I('post.handleType') == "AddGroup"?$groupID=$this->AddNewsGroup():$groupID=I('post.groupid');

            for ( $i = 0;$i<count( $items );$i++ ) {
                if($items[$i]['title'] == "" || $items[$i]['image'] == "" || $items[$i]['content'] == ""){
                    $result['status'] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result['info'] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                    $this->ajaxReturn($result , "JSON");
                }
                
                $data = array(
                    'ecid' => session($this->_userCfg['ECID']) , 
                    'title' => $items[$i]['title'],
                    'modifyTime' => date( "Y-m-d H:i:s" ),
                    'modifyUserId' => session($this->_userCfg['UID']) , 
                    'description' => $items[$i]['description'],
                    'content' => $items[$i]['content'],
                    'bigImg' => $items[$i]['image'],
                    'minImg' => $items[$i]['image'],
                    'addPicture' => I('post.addPicture')
                );

                if ( $newsid = M( "Company_news" )->add( $data ) ) {
                    $this->bindGroup( $newsid , $groupID );
                    $this->setMaterialSession($groupID);
                }
            }

            $result['status'] = Error::SUCCESS_OK;
            $this->ajaxReturn( $result , "JSON" );
        }
    }

    private function setMaterialSession($materialID){
        $webPath = "http://115.29.37.72";
        $m = M( 'Company_material_group' );
        $opt['id'] = $materialID;
        $opt['ecid'] = session( $this->_userCfg['ECID'] );
        $result = $m->where( $opt )->find();
        $arrMaterialID = explode( ',', $result['materialId'] );

        for ( $i=0; $i<count( $arrMaterialID ); $i++ ) {
            $m = M( 'Company_news' );
            $condition['id'] = $arrMaterialID[$i];
            $result = $m->where( $condition )->find();

            $picUrl = ($i==0)?$result['bigImg']:$result['minImg'];

            if(substr($picUrl,0,4) != 'http')
                $picUrl = \Weixin\Response\WechatConst::SERVER_DOMAIN . $picUrl;

            $reply['type'] = "news";
            $reply['content'][$i] = array(
                'Title'=>$result['title'],
                'Description'=>$result['description'],
                'PicUrl'=> $picUrl,
                'Url'=> \Weixin\Response\WechatConst::SERVER_DOMAIN . '/Index/news/id/'.$condition['id'] );
        }

        S("Material_".$materialID , $reply);
    }

    private function bindGroup( $newsid , $groupid ) {
        $groupResult = M( "Company_material_group" )->where( 'id = ' . $groupid )->find();

        //判断是否有materialID存在，如果有则添加逗号
        if ( $groupResult['materialId'] == "" ) {
            $groupData = array(
                'materialId' => $newsid
            );
        }
        else {
            $groupData = array(
                'materialId' => $groupResult['materialId'] .= ",".$newsid
            );
        }

        return M( "Company_material_group" )->data( $groupData )->where( 'id = ' . $groupid )->save();
    }

    public function getAddDiv() {
        $id = I('get.id');
        $key = I('get.key');
        $data['data'] = "<div class='news-list' id='list-$key' style='min-height:100px;' onmouseover=\"showEdit($key)\" onmouseout=\"outEdit($key)\">
                        <div id='title-$key' class='news-title' style='min-height:70px;'>标题</div>
                        <div class='news-img'><span><img id='img-$key' src=''/></span></div>
                        <div class='news-edit' id='news-$key' style='min-height:70px;'><div id='news-a'><a onclick=\"setSelID($key, newsArr, getImgPath(newsArr.items[$id].image, 's'))\"><img src='".__ROOT__."/Public/Image/edit.png' /></a><a href='javascript:delItem();'><img src='".__ROOT__."/Public/Image/delete.png'/></a></div></div>
                        <div style='clear:both'></div></div>";

        $data['status'] = 0;
        $data['key'] = $key;

        $this->ajaxReturn( $data , 'JSON');
    }

    public function deleteNews() {

        if ( M( "company_".session( "ecid" )."_material" )->where( "id=".$_GET["newsid"] )->delete() ) {
            $groupResult = M( "company_".session( "ecid" )."_material_group" )->where( "id=".$_GET["groupid"] )->select();
            $groupid = explode( ",", $groupResult[0]["materialID"] );

            $id = $groupid[0];
            for ( $i = 1;$i < count( $groupid );$i++ ) {
                if ( $groupid[$i] != $_GET["newsid"] ) {
                    $id .= ",".$groupid[$i];
                }
            }
        }

        $data = array(
            "materialID" => $id
        );

        if ( M( "company_".session( "ecid" )."_material_group" )->data( $data )->where( "id=".$_GET["groupid"] )->save() ) {
            $this->success( '修改成功' , 'EditNews?groupid='.$_GET["groupid"] );
        }
        else {
            $this->error( '修改失败' );
        }
    }

    public function EditNews() {
        $groupResult = M( "Company_material_group" )->where( "id=".$_GET["groupid"] )->find();
        $newsIds = explode( ",", $groupResult["materialId"] );
        $newsArr['selID'] = 0;
        $newsArr['groupid'] = $_GET["groupid"];
        $newsArr['total'] = count( $newsIds );
        $newsArr['delIds'] = '';

        for ( $j = 0;$j<count( $newsIds );$j++ ) {
            $newsResult = M( "Company_news" )->where( 'id = ' . $newsIds[$j] )->find();

            $newsArr['items'][$j] = array(
                'id' => $newsResult['id'],
                'key' => $j,
                'title' => $newsResult['title'],
                'modifyTime' => $newsResult['modifyTime'],
                'description' => $newsResult['description'],
                'content' => htmlspecialchars_decode($newsResult['content']),
                'image' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg'],
                'update'=> 0,
                'synWeb'=>$newsResult['synWeb'],
                'addPicture'=>$newsResult['addPicture']
            );
        }

        $this->assign( 'userName', session( 'user_name' ) );//用户名
        $this->assign( "itemArrJson" , json_encode( $newsArr ) );
        $this->assign( "itemArr" , $newsArr );
        $this->assign( "itemTime" , date( "Y-m-d", strtotime( $groupResult['modifyTime'] ) ) );

        $this->setToken();
        $this->display();
    }

    public function ModifyHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $items = $_POST["items"];
        
        if ( $items != '' ) {
            for ( $i = 0;$i<count( $items );$i++ ) {
                $opt['ecid'] = session($this->_userCfg['ECID']);
                $opt['title'] = $items[$i]['title'];
                $opt['id'] = array("NEQ" , $items[$i]['id']);
                if($this->isDataExist('Company_news' , $opt)){
                    $result['status'] = Error::ERROR_NEWS_NAME_EXIST;
                    $result['info'] = Error::getErrMsg(Error::ERROR_NEWS_NAME_EXIST);
                    $this->ajaxReturn($result , "JSON");
                }

                if($items[$i]['title'] == "" || $items[$i]['image'] == "" || $items[$i]['content'] == ""){
                    $result['status'] = Error::ERROR_EDIT_HANDLE_ERR;
                    $result['info'] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
                    $this->ajaxReturn($result , "JSON");
                }

                if ( $items[$i]['update'] == 1 ) {
                    $img = $items[$i]['image'];

                    $data = array(
                        'id' => $items[$i]['id'],
                        'title' => $items[$i]['title'],
                        'modifyTime' => date( "Y-m-d H:i:s" ),
                        'modifyUserId' => session($this->_userCfg['UID']) , 
                        'description' => $items[$i]['description'],
                        'content' => $items[$i]['content'],
                        'bigImg' => $img,
                        'minImg' => $img,
                        // 'synWeb' => $items[$i]['synWeb'],
                        'addPicture'=>$items[$i]['addPicture']
                    );
                    M( "Company_news" )->data( $data )->save();

                }

                if ( $items[$i]['update'] == 2 ) {
                    $data = array(
                        'ecid' => session($this->_userCfg['ECID']) , 
                        'title' => $items[$i]['title'],
                        'modifyTime' => date( "Y-m-d H:i:s" ),
                        'modifyUserId' => session($this->_userCfg['UID']) , 
                        'description' => $items[$i]['description'],
                        'content' => $items[$i]['content'],
                        'bigImg' => $items[$i]['image'],
                        'minImg' => $items[$i]['image'],
                        'synWeb' => $items[$i]['synWeb'],
                        'addPicture'=>$items[$i]['addPicture']
                    );
                    $items[$i]['id'] = M( "Company_news" )->data( $data )->add();

                }

                if ( $_POST['delIds'] != '' ) {
                    $arrIds = explode( '-' , $_POST['delIds'] );

                    for ( $i=0; $i<count( $arrIds ); $i++ ) {
                        if ( $arrIds[$i] != '' ) {
                            M( "Company_news" )->delete( $arrIds[$i] );
                        }
                    }
                }
            }


            $materialIds = $items[0]['id'];
            for ( $i=1; $i<count( $items ); $i++ ) {
                $materialIds .= ',' . $items[$i]['id'];
            }
            $data = array(
                'id' => $_POST['groupid'],
                'ecid' => session($this->_userCfg['ECID']) , 
                'modifyTime' => date( "Y-m-d H:i:s" ),
                'modifyUserId' => session($this->_userCfg['UID']) , 
                'materialId' => $materialIds
            );
            $t = M( "Company_material_group" )->data( $data )->save();
            $this->setMaterialSession($_POST['groupid']);
            $result['status'] = Error::SUCCESS_OK;
            $this->ajaxReturn($result , "JSON");
        }
    }

    public function deleteGroup() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        
        if($row = M("Company_event_response")->where("responseType = 'news' AND ecid = '".session($this->_userCfg['ECID'])."'")->find())
        {
            if($row['responseMaterialId'] == I('post.id')){
                $arr['responseType'] = 'text';
                 M("Company_event_response")->where("id = ".$row['id'])->save($arr);  
            }
        }

        $newsid = M( "Company_material_group" )->where( "id=".$_POST["id"]." AND ecid = '".session($this->_userCfg['ECID'])."'")->select();
        $newsidArr = explode( ",", $newsid[0]["materialId"] );

        for ( $i = 0;$i < count( $newsidArr );$i++ ) {
            M( "Company_news" )->where( "id=".$newsidArr[$i]." AND ecid = '".session($this->_userCfg['ECID'])."'")->delete();
        }

        if ( M( "Company_material_group" )->where( "id=".$_POST["id"]." AND ecid = '".session($this->_userCfg['ECID'])."'")->delete() ) {
            S("Material_".$_POST["id"] , null);

            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    private function isDataExist($table, $options){
        $m = M($table);

        return $m->where($options)->find();
    }
}
?>
