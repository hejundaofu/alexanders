<?php
namespace Admin\Action;
use Org\Error\Error;

class OrderAction extends AdminAction{
	public function index(){
		if(session( 'user_name' ) == 'check')
			$this->display('check');
		else
			$this->display();
	}

	public function orderInfo(){
		$id = I('get.id');
		$orderInfo = $this->getOrderInfo($id);

		if($orderInfo['sign'] == 1){
			$this->assign('sendUser', $this->getSendUser($orderInfo['city']));
		}
		$this->assign('orderInfo', $orderInfo);
		$this->assign('userInfo', $this->getUserInfo($orderInfo['uId']));
		$this->display();
	}

	public function orderList(){
		$Data = M('Company_order');

		$params = I('post.');

		$opt = array();

		switch($params['searchType']){
			case 'orderNo':
				if(session( 'user_name' ) == 'check')
					$opt['orderNo'] = $params['keyword'];
				else
					$opt['orderNo'] = array('LIKE', '%'.$params['keyword'].'%');
				break;
			case 'name':
				if(strlen($params['keyword'])==0){
					$opt['address'] = ' ';
				}
				$opt['address'] = array('LIKE', '%"name"%'.$params['keyword'].'%');
				break;
			case 'tel':
				if(strlen($params['keyword'])==0){
					$opt['address'] = ' ';
				}else if(strlen($params['keyword'])<4){
					$opt['address'] = $params['keyword'];
				}else
					$opt['address'] = array('LIKE', '%"tel"%'.$params['keyword'].'%');
				break;
			default:
				$opt = $params;
				break;
		}

		$city = session($this->_userCfg['CITY']);

		if($city != '')
			$opt['city'] = $city;


		$count      = $Data->where($opt)->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $order = $Data->where($opt)->order( 'addTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        if($order){
        	for($i=0; $i<count($order); $i++){
        		$order[$i]['payType'] = $this->getPayType($order[$i]['payType']);

        		$order[$i]['paySign'] = $this->getPaySign($order[$i]['payTime']);

        		$order[$i]['wlSign'] = $this->getWlSign($order[$i]['sign']);

        		$order[$i]['userInfo'] = unserialize($order[$i]['address']);
        		$order[$i]['goods'] = unserialize($order[$i]['goods']);
        	}
        }

        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign('order', $order);
        $this->assign('params', $opt);
        $this->display();
	}

	public function addAdminMessage(){
		$Data = M('Company_order');

		$orderInfo = $Data->find(I('post.id'));

		$this->assign('orderInfo', $orderInfo);
		$this->display();
	}

	public function outToExcel(){
		// 输出Excel文件头，可把user.csv换成你要的文件名  
		header("Content-type:text/csv");
		header('Content-Disposition: attachment;filename="user.csv"');  
		header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
	    header('Expires:0');
	    header('Pragma:public');    
		// 从数据库中获取数据，为了节省内存，不要把数据一次性读到内存，从句柄中一行一行读即可  
		$m = M('Company_order');
		$city = session($this->_userCfg['CITY']);

		if($city != '')
			$opt['city'] = $city;

		$opt['sendDay'] = I('get.t');

		$result = $m->where($opt)->select(); 
		// 打开PHP文件句柄，php://output 表示直接输出到浏览器  
		$fp = fopen('php://output', 'a');    
		// 输出Excel列名信息  
		$head = array('订单号', '收件人', '联系电话', '城市', '行政区','收件地址', '配送日期','配送时间','口味','微信支付','金额','货到付款','客户留言','管理留言','备注','配送员');  
		foreach ($head as $i => $v) {  
		    // CSV的Excel支持GBK编码，一定要转换，否则乱码  
		    $head[$i] = iconv('utf-8', 'gbk', $v);  
		}    
		//将数据通过fputcsv写到文件句柄  
		fputcsv($fp, $head);   
		// 计数器  
		$cnt = 0;  
		// 每隔$limit行，刷新一下输出buffer，不要太大，也不要太小  
		$limit = 100000;    
		// 逐行取出数据，不浪费内存  
		for($j=0; $j<count($result); $j++){
		    $row= $result[$j];
		    // foreach ($row as $i => $v) {  
		    //     $row[$i] = iconv('utf-8', 'gbk', $v);  
		    // }  
		    $city = $row['city'];
		    $orderNo = "'".$row['orderNo'];
		    $userInfo = unserialize($row['address']);
		    $goods = unserialize($row['goods']); 
		    $goodsText = '';
		    for($i=0; $i<count($goods); $i++){
	            if($i>0)
	                $goodsText .= ',';
	            $goodsText .= $goods[$i]['name'] . $goods[$i]['num'];
	        }
		    $sendDay = $row['sendDay'];
		    $sendTime = $row['sendTime'];

		    if($row['payTime'] != ''){
		    	$payType = '微信支付';
		    	$payPrice = $row['payPrice'];
		    }else{
		    	$payType = '';
		    	$payPrice = '';
		    }

		    if($row['payType'] == 'nopay'){
		    	$nopay = $row['payPrice'];
		    }else{
		    	$nopay = '';
		    }

		    if($row['sign'] == 0){
		    	$sign = '未完成订单';
		    }else{
		    	$sign = '';
		    }

		    $info = array(
		    	iconv('utf-8', 'gbk', $orderNo),
		    	iconv('utf-8', 'gbk', $userInfo['name']),
		    	iconv('utf-8', 'gbk', $userInfo['tel']),
		    	iconv('utf-8', 'gbk', $city),
		    	iconv('utf-8', 'gbk', $userInfo['district']),
		    	iconv('utf-8', 'gbk', $userInfo['address']),
		    	iconv('utf-8', 'gbk', $sendDay),
		    	iconv('utf-8', 'gbk', $sendTime),
		    	iconv('utf-8', 'gbk', $goodsText),
		    	iconv('utf-8', 'gbk', $payType),
		    	iconv('utf-8', 'gbk', $payPrice),
		    	iconv('utf-8', 'gbk', $nopay),
		    	iconv('utf-8', 'gbk', $row['Message']),
		    	iconv('utf-8', 'gbk', $row['adminMessage']),
		    	iconv('utf-8', 'gbk', $sign),
		    	iconv('utf-8', 'gbk', ''),
		    	);
		    fputcsv($fp, $info);  
		}
	}

	public function quickChange(){
		$Data = M('Company_order');

		$orderInfo = $Data->find(I('post.id'));

		if($orderInfo['sign'] == 1){
			$this->assign('sendUser', $this->getSendUser($orderInfo['city']));
		}

		$this->assign('orderInfo', $orderInfo);
		$this->display();
	}

	public function mapMark(){
		$id = I('post.id');
		$orderInfo = $this->getOrderInfo($id);

		dump($orderInfo);
		$this->assign('orderInfo', $orderInfo);
		$this->display();
	}

	public function orderHandle(){
		$orderInfo = I('post.');

		if($orderInfo['sign'] == 1){
			$m = M('Company_employees');

			$employees = $m->find($orderInfo['allocationInfo']['sendUserId']);

			$opt['id'] = $orderInfo['id'];
			$opt['sign'] = 2;
			$opt['allocationInfo'] = array(
				'id' => $employees['id'],
				'name' => $employees['name'],
				'tel' => $employees['tel'],
				'time' => date('Y-m-d H:i:s')
				);

			$opt['allocationInfo'] = json_encode($opt['allocationInfo']);

			$data = M('Company_order');
			$data->save($opt);

			$req['Code'] = 0;
			$req['Message'] = '成功';

			$orderInfo['allocationInfo'] = $opt['allocationInfo'];
			$orderInfo['sign'] = 2;

			$this->sendCustomMsg($orderInfo['id']);

			$this->ajaxReturn($req, 'JSON');
		}

		if($orderInfo['sign'] == 2){
			$opt['id'] = $orderInfo['id'];
			$opt['sign'] = 3;

			$data = M('Company_order');
			$data->save($opt);

			$req['Code'] = 0;
			$req['Message'] = '成功';
			$this->ajaxReturn($req, 'JSON');
		}
	}

	public function mapAll(){
		$m = M('Company_order');
		$opt['sendDay'] = date('Y-m-d');
		$opt['mapMark'] = array('neq', '');

		$result = $m->where($opt)->select();

		if(!$result){
			echo '今日无已标记位置的订单';
			return;
		}

		for($i=0; $i<count($result); $i++){
			$result[$i]['mapMark'] = json_decode($result[$i]['mapMark'], true);
			$result[$i]['address'] = unserialize($result[$i]['address']);
			$result[$i]['wlSign'] = $this->getWlSign($result[$i]['sign']);
		}

		$this->assign('orderInfo', $result);
		$this->display();
	}

	public function saveOrderHandle(){
		$opt = I('post.');

		if($opt['mapMark'])
			$opt['mapMark'] = json_encode($opt['mapMark']);

		$m = M('Company_order');
		$result = $m->save($opt);
		if($result){
			$req['Code'] = 0;
			$req['Message'] = '成功';
		}else{
			$req['Code'] = -1;
			$req['Message'] = '失败';
		}

		$this->ajaxReturn($req, 'JSON');
	}

	private function getCompanyInfo(){
        $m = M('Company_info');

        return $m->find(888);
    }

	private function sendCustomMsg($id){
		$m = M('Company_order');
		$orderInfo = $m->find($id);
        $userInfo = $this->getUserInfo($orderInfo['uId']);

        $goods = unserialize($orderInfo['goods']);
        $sendUser = json_decode($orderInfo['allocationInfo'], true);
        for($i=0; $i<count($goods); $i++){
            if($i>0)
                $goodsText .= ',';
            $goodsText .= $goods[$i]['name']. '*' . $goods[$i]['num'];
        }

        $address = unserialize(($orderInfo['address']));

        $title = "您好，你的订单【{$orderInfo['orderNo']}】已配送，请注意查收。";

        if($userInfo){
            $companyInfo = $this->getCompanyInfo();

            $options = array(
                'appid'      =>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
                'appsecret'  =>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
                'partnerid'  =>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
                'partnerkey' =>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
                'paysignkey' =>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
            );

            $sendMsg = "配送人员：{$sendUser['name']};联系电话：{$sendUser['tel']}";

            $data = array(
                'touser' => $userInfo['openId'],
                'template_id' => 'vjn3tEx2CteT39U5fylZd4oZGr3y606bcm-q_HS02iY',
                'url' => 'http://www.icalex.com',
                'topcolor' => '#000000',
                'data' => array(
                    'first' => array(
                        'value' => $title,
                        "color"=>"#173177"
                        ),
                    'keyword1' => array(
                        'value' => $orderInfo['orderNo'],
                        "color"=>"#173177"
                        ),
                    'keyword2' => array(
                        'value' => $orderInfo['addTime'],
                        "color"=>"#173177"
                        ),
                    'keyword3' => array(
                        'value' => $orderInfo['payPrice'].'元',
                        "color"=>"#173177"
                        ),
                    'keyword4' => array(
                        'value' => $address['name'],
                        "color"=>"#173177"
                        ),
                    'keyword5' => array(
                        'value' => $address['address'],
                        "color"=>"#173177"
                        ),
                    'remark' => array(
                        'value' => $sendMsg,
                        "color"=>"#173177"
                        )
                    )
                );
            $wechat = new \Org\Weixin\Wechat($options);
            $wechat->sendTemplateMessage($data);
        }
	}

	private function getSendUser($city){
		$m = M('Company_employees');

		$opt['ecid'] = 888;
		$opt['city'] = $city;

		return $m->where($opt)->select();
	}

	private function getUserInfo($uId){
		$m = M('Company_888_user_info');

		return $m->find($uId);
	}

	private function getOrderInfo($id){
		$Data = M('Company_order');
		$result = $Data->find($id);

		$result['payType'] = $this->getPayType($result['payType']);

       	$result['paySign'] = $this->getPaySign($result['payTime']);

       	$result['wlSign'] = $this->getWlSign($result['sign']);

       	$result['goods'] = unserialize($result['goods']);
       	$result['address'] = unserialize($result['address']);

       	return $result;
	}

	private function getPayType($payCode){
		$payment = '';
		switch ($payCode) {
			case 'wechatpay':
				$payment = '微信支付';
				break;
			
			default:
				$payment = '货到付款';
				break;
		}

		return $payment;
	}

	private function getWlSign($sign){
		$wlSign = '';
		
		switch($sign){
			case 1:
				$wlSign = '待配送';
				break;
			case 2:
				$wlSign = '配送中';
				break;
			case 3:
				$wlSign = '已完成';
				break;
			default:
				$wlSign = '未完成订单';
				break;
		}

		return $wlSign;
	}

	private function getPaySign($payTime){
		$paySign = '';
		
		if($payTime != '')
			$paySign = "<span class='text-success'>已支付</span>";
		else
			$paySign = "<span class='text-danger'>未支付</span>";

		return $paySign;
	}
}