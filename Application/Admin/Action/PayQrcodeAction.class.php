<?php
namespace Admin\Action;
use Weixin\Response\WechatConst;
use Org\Error\Error;
use Think\Action;

class PayQrcodeAction extends AdminAction {
	public function index(){
		$this->display();
	}

	public function qrcodeList(){
		$Data = M('Company_pay_qrcode');
		$count      = $Data->count();// 查询满足要求的总记录数 $map表示查询条件
        $page       = new \Think\Page( $count , 15 );// 实例化分页类 传入总记录数
        $show       = $page->show();// 分页显示输出
        // 进行分页数据查询
        $order = $Data->order( 'addTime desc' )->limit( $page->firstRow.','.$page->listRows )->select();

        $this->assign( 'page', $show );// 赋值分页输出
        $this->assign('qrcodeList', $order);
        $this->display();
	}

	public function getQrImg(){
		$this->assign('url', I('get.id'));
		$this->display();
	}

	public function qrcode(){
        $payQrcodeId = I('get.id');
        $m = M('Company_pay_qrcode');
        $result = $m->find($payQrcodeId);

        if($result){
            $companyInfo = $this->getCompanyInfo();
            if($companyInfo){
                $options = array(
                    'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
                    'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
                    'partnerid'=>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
                    'partnerkey'=>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
                    'paysignkey'=>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
                );
                $wechat = new \Org\Weixin\Wechat($options);

                $url = $wechat->createNativeUrl('pay_'.$payQrcodeId);

                Vendor('phpQrcode.phpqrcode');

                $errorCorrectionLevel="L";
                $matrixPointSize="4";
                \QRcode::png($url,false,$errorCorrectionLevel,$matrixPointSize);
            }
        }
    }

    private function getCompanyInfo(){
        $m = M('Company_info');

        return $m->find(888);
    }
}