<?php
namespace Admin\Action;
use Org\Error\Error;
class WallAction extends Action {
    public function index(){
        $ecid       =$_GET['ecid'];
        $activityId =$_GET['activityId'];
        
        $this->assign('ecid',$ecid);
        $this->assign('activityId',$activityId);
        $this->display();
    }

    public function addHandle(){
        

        $id=$_POST['id'];
        $activityId = $_POST['activityId'];
        $ecid = $_POST['ecid'];
        $Model = new Model();
        $result = $Model->query("
            SELECT 
            user_info.id,
            user_info.nickname,
            user_info.headimgurl
            FROM sz12365_fw_company_qr_sign_user_openid_{$activityId} AS user_openid
            JOIN sz12365_fw_company_{$ecid}_user_info AS user_info ON
            user_openid.openid=user_info.openId
            Where user_info.id > {$id} 
            LIMIT 0,1
             ");
        if($result){
            $data = $result[0];
            $data['status'] =ERROR::SUCCESS_OK;
        }
        else{
            $data['status'] =ERROR::ERROR_EDIT_HANDLE_ERR;
            $data['info'] = ERROR::getErrMsg(ERROR::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($data , "JSON");
    }
}
?>