<?php
namespace Admin\Action;
use Org\Error\Error;
class AllocationAction extends AdminAction {
	public function index(){
		$m = M('Company_city');
		$result = $m->select();

		$this->assign('cityList', $result);
		$this->display();
	}

	public function cityEdit(){
		$id = I('post.id');
		$m = M('Company_city');
		$result = $m->find($id);

		$this->assign('cityInfo', $result);
		$this->display();
	}

	public function editCityHandle(){
		$cityInfo = I('post.');

		if($cityInfo){
			$m = M('Company_city');
			if($cityInfo['id']){
				$result = $m->save($cityInfo);

				if($result){
					$req['Code'] = 0;
					$req['Message'] = '修改成功！';
				}else{
					$req['Code'] = -1;
					$req['Message'] = '修改失败！';
				}
			}else{
				$result = $m->add($cityInfo);

				if($result){
					$req['Code'] = 0;
					$req['Message'] = '添加成功！';
				}else{
					$req['Code'] = -1;
					$req['Message'] = '添加失败！';
				}
			}
		}

		$this->ajaxReturn($req);
	}
}
