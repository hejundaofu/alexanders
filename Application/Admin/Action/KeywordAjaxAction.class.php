<?php

class KeywordAjaxAction extends PublicAction {
    public function editKey() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        if ( M( "company_".session( "ecid" )."_keyword_response" )->data( $_POST )->where( 'id = ' . $_POST["id"] )->save() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function addKey() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $_POST["addTime"] = date( "Y-m-d H:i:s" );
        if ( M( "company_".session( "ecid" )."_keyword_response" )->data( $_POST )->add() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function deleteKey() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        if ( M( "company_".session( "ecid" )."_keyword_response" )->data( $_POST )->where( 'id='.$_POST["id"] )->delete() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function updateEvent() {
        

        if ( M( "company_event_response" )->data( $_POST )->where( "ecid = '".session( "ecid" )."'" )->save() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else {
            $result["status"] = Error::ERROR_EDIT_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_EDIT_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");
    }

    public function addEvent() {
        

        $_POST["ecid"] = session( "ecid" );
        if ( M( "company_event_response" )->data( $_POST )->add() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else {
            $result["status"] = Error::ERROR_ADD_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_ADD_HANDLE_ERR);
        }

        $this->ajaxReturn($result , "JSON");
    }

    public function eventHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        $data = array(
            "eventType" => $_POST["eventType"] ,
            "ecid" => session( "ecid" )
        );

        if ( M( "company_event_response" )->where( $data )->find() ) {
            $this->updateEvent();
        }else {
            $this->addEvent();
        }
    }
}
?>
