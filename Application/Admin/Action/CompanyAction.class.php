<?php
namespace Admin\Action;
use Org\Error\Error;
class CompanyAction extends AdminAction {
    public function eat(){
        $m = M('Company_try_eat');
        $result = $m->select();
        $this->assign('tryList', $result);
        $this->display();
    }

    public function info(){
        $m = M('Company_try_eat');
        $result = $m->find(I('get.id'));
        $this->assign('item', $result);
        $this->display();
    }

    public function outToExcel(){
		// 输出Excel文件头，可把user.csv换成你要的文件名  
		header("Content-type:text/csv");
		header('Content-Disposition: attachment;filename="company.csv"');  
		header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
	    header('Expires:0');
	    header('Pragma:public');    
		// 从数据库中获取数据，为了节省内存，不要把数据一次性读到内存，从句柄中一行一行读即可  
		$m = M('Company_try_eat');

		$result = $m->select();
		// 打开PHP文件句柄，php://output 表示直接输出到浏览器  
		$fp = fopen('php://output', 'a');    
		// 输出Excel列名信息  
		$head = array('企业名称', '城市', '区域', '地址', '试吃部门','人数', '试吃时间','企业需求','联系人','职位','办公电话','手机','邮箱','留言','申请日期');  
		foreach ($head as $i => $v) {  
		    // CSV的Excel支持GBK编码，一定要转换，否则乱码  
		    $head[$i] = iconv('utf-8', 'gbk', $v);  
		}    
		//将数据通过fputcsv写到文件句柄  
		fputcsv($fp, $head);   
		// 计数器  
		$cnt = 0;  
		// 每隔$limit行，刷新一下输出buffer，不要太大，也不要太小  
		$limit = 100000;    
		// 逐行取出数据，不浪费内存  
		for($j=0; $j<count($result); $j++){
		    $row= $result[$j];

		    $info = array(
		    	iconv('utf-8', 'gbk', $row['companyName']),
		    	iconv('utf-8', 'gbk', $row['companyCity']),
		    	iconv('utf-8', 'gbk', $row['companyArea']),
		    	iconv('utf-8', 'gbk', $row['companyAddress']),
		    	iconv('utf-8', 'gbk', $row['companyDepartment']),
		    	iconv('utf-8', 'gbk', $row['eatPeople']),
		    	iconv('utf-8', 'gbk', $row['eatTimerand']),
		    	iconv('utf-8', 'gbk', $row['companyDemand']),
		    	iconv('utf-8', 'gbk', $row['userName']),
		    	iconv('utf-8', 'gbk', $row['userPosition']),
		    	iconv('utf-8', 'gbk', $row['userTel']),
		    	iconv('utf-8', 'gbk', $row['userMobile']),
		    	iconv('utf-8', 'gbk', $row['userEmail']),
		    	iconv('utf-8', 'gbk', $row['userMessage']),
		    	iconv('utf-8', 'gbk', $row['addTime']),
		    	);
		    fputcsv($fp, $info);  
		}
	}
}
?>
