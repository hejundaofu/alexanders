<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class LocationAjaxAction extends PublicAction{
    public function addPoiHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'poi' ) );
        

        import( '@.BaiduMap.BaiduMap' );
        $baiduMap = new BaiduMap( session( "ecid" ) );

        $_POST["ecid"] = session( "ecid" );

        $data = array(
            "tel" => $_POST["tel"] ,
            "info" => $_POST["info"] ,
            "bigImg" => $_POST["bigImg"] ,
            "smallImg" => $_POST["smallImg"]
        );

        $response = $baiduMap->addPoi( $_POST , $data );

        if ( $response ) {
            $this->ajaxReturn( $response );
        }

        $this->ajaxReturn( $response );
    }

    public function updatePoiHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'poi' ) );
        
        import( '@.BaiduMap.BaiduMap' );

        $baiduMap = new BaiduMap( session( "ecid" ) );

        $data = array(
            "longitude" => $_POST["original_lon"] ,
            "latitude" => $_POST["original_lat"] ,
            "tel" => $_POST["tel"] ,
            "name" => $_POST["title"] ,
            "poiID" => $_POST["poiID"] ,
            "info" => $_POST["info"] ,
            "bigImg" => $_POST["bigImg"] ,
            "smallImg" => $_POST["smallImg"] ,
            "address" => $_POST["address"]
        );

        $response = $baiduMap->modifyPoi( $_POST["poiID"] , $_POST , $data );

        $this->ajaxReturn( $response );
    }

    public function deletePoi() {
        import( '@.BaiduMap.BaiduMap' );
        

        $baiduMap = new BaiduMap( session( "ecid" ) );

        $response = $baiduMap->delPoi( $_POST["id"] );

        if ( $response->status == 0 ) {
            $data = array(
                "poiID" => $_POST["id"],
                "ecid" => session( "ecid" )
            );
            M( "Company_location_set" )->where( $data )->delete();
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }

    public function allDelete() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'poi' ) );
        
        import( '@.BaiduMap.BaiduMap' );
        
        $baiduMap = new BaiduMap( session( "ecid" ) );

        $response = $baiduMap->delPoi( $_POST["id"] );

        if ( $response->status == 0 ) {
            $arr = explode( ",", $_POST["id"] );
            for ( $i = 0;$i<count( $arr );$i++ ) {
                M( "Company_location_set" )->where( "poiID = '".$arr[$i]."'" )->delete();
            }
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }
}
?>
