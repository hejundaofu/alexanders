<?php
class MenuAjaxAction extends PublicAction {
    public function menuHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        import( '@.WechatMenu.MenuDbControl' );
        

        $menuControl = new MenuDbControl( session( "ecid" ) );

        $code = $menuControl->updateWechatMenu();

        $this->ajaxReturn( $code, Error::getErrMsg( $code ) , 0 );
    }

    public function checkWeixinHandle(){
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $result = M("Company_info")->where("company_ecid = '".session("ecid")."'")->find();

        if($result["weixin_AppId"] == null || $result["weixin_access"] == null){
            $data = array(
                "status" => Error::ERROR_WECHATMENU_INVALID_APPID , 
                "info" => Error::getErrMsg(Error::ERROR_WECHATMENU_INVALID_APPID)
                );
        }else{
           $data = array(
                "status" => Error::SUCCESS_OK , 
                "info" => Error::getErrMsg(Error::SUCCESS_OK)
                ); 
        }

        $this->ajaxReturn($data , "JSON");
    }

    public function addMenu() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        import( '@.WechatMenu.MenuDbControl' );

        $_POST["ecid"] = session( "ecid" );

        $menuControl = new MenuDbControl( session( "ecid" ) );
        $result = $menuControl->addMenuItem( $_POST );

        $this->ajaxReturn( $result );
    }

    public function updateMenuList() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        import( '@.WechatMenu.MenuDbControl' );
        

        $_POST["ecid"] = session( "ecid" );
        $menuControl = new MenuDbControl( session( "ecid" ) );

        $code = $menuControl->updateMenuItem( $_POST );

        $this->ajaxReturn( $code, Error::getErrMsg( $code ) , 0 );
    }

    public function getMaterialGroup() {
        $page = $_GET["page"]-1;
        if ( $page == "" || $page < 0 )$page=0;
        $result = $this->getMaterial( $page*4, 4 );

        for ( $i = 0;$i<count( $result );$i++ ) {
            $news = "";
            for ( $j = 1;$j<count( $result[$i]["Result"] );$j++ ) {
                $news .= "
                <div class='news-list'>
              <div class='news-title'>".$result[$i]["Result"][$j]["title"]."</div>
              <div class='news-img'><img src=\"".$result[$i]["Result"][$j]['newsImg']."\" onload=\"javascript:DrawImage(this,'70','70');\"/></div>
          </div>
                ";
            }

            $div .= "
                <div id='news-".$result[$i]["groupId"]."' style='cursor:pointer;'>
                    <div class='news' onclick=\"setMaterial('".$result[$i]["groupId"]."' , '".$_POST["type"]."' , '".$_POST["index"]."' , '".$_POST["listIndex"]."')\">
                        <div class='news-big'>
                            <div class='news-big-time'>".date( "Y-m-d", strtotime( $result[$i]['addTime'] ) )."</div>
                            <div class='news-big-img'><img src='".$result[$i]['Result'][0]['newsImg']."' onload=\"javascript:DrawImage(this,'330','160');\"/></div>
                            <div class='news-big-title'>".$result[$i]['Result'][0]['title']."</div>
                    </div>
                    ".$news."
                    </div>
                </div>
            ";
        }
        $this->ajaxReturn( $div );
    }

    private function getMaterial( $start , $end ) {
        $groupResult = M( "company_".session( "ecid" )."_material_group" )->limit( $start.",".$end )->select();

        $newsArr = null;     //记录数据库数据
        for ( $i = 0;$i<count( $groupResult );$i++ ) {
            $newsArr[$i]["groupId"] = $groupResult[$i]['id'];
            $newsArr[$i]['addTime'] = $groupResult[$i]['addTime'];
            $newsId = explode( ",", $groupResult[$i]["materialID"] );

            $newsRow = null;
            for ( $j = 0;$j<count( $newsId );$j++ ) {
                $newsResult = M( "company_".session( "ecid" )."_material" )->where( 'id = ' . $newsId[$j] )->find();

                $newsRow[$j] = array(
                    'title' => $newsResult['title'],
                    'addTime' => $newsResult['addTime'],
                    'description' => $newsResult['description'],
                    'content' => $newsResult['content'],
                    'newsImg' => ( $j==0 )?$newsResult['bigImg']:$newsResult['minImg']
                );
            }
            $newsArr[$i]['Result'] = $newsRow;
        }

        return $newsArr;
    }

    public function getNewsDiv( $id ) {
        $data = M( "company_".session( "ecid" )."_material_group" )->where( 'id = ' . $id )->find();

        $newsId = explode( ",", $data["materialID"] );

        $newsRow = null;
        for ( $i = 0;$i<count( $newsId );$i++ ) {
            $newsResult = M( "company_".session( "ecid" )."_material" )->where( 'id = ' . $newsId[$i] )->find();

            $newsRow[$i] = array(
                'title' => $newsResult['title'],
                'addTime' => $newsResult['addTime'],
                'description' => $newsResult['description'],
                'content' => $newsResult['content'],
                'newsImg' => ( $i==0 )?$newsResult['bigImg']:$newsResult['minImg']
            );
            $data["Result"] = $newsRow;
        }

        for ( $i = 1;$i<count( $data["Result"] );$i++ ) {
            $news .= "
                <div class='news-list'>
              <div class='news-title'>".$data["Result"][$i]["title"]."</div>
              <div class='news-img'><img src=\"".$data["Result"][$i]['newsImg']."\" onload=\"javascript:DrawImage(this,'70','70');\"/></div>
          </div>
                ";
        }

        $div = "<div id='data-news'>
                                    <div class='news'>
                <div class='news-big'>
                <div class='news-big-time'>".date( "Y-m-d", strtotime( $data['addTime'] ) )."</div>
                <div class='news-big-img'><img src=\"".$data['Result'][0]['newsImg']."\" onload=\"javascript:DrawImage(this,'330','160');\"/></div>
                <div class='news-big-title'>".$data['Result'][0]['title']."</div>
            </div>
        ".$news."</div></div>";

        $this->ajaxReturn( $div );
    }

    public function deleteSubMenu() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        if ( M( "Company_menu" )->where( "id = ".$_POST["id"]."" )->delete() ) {
            $this->ajaxReturn( 0 );
        }else {
            $this->ajaxReturn( -1 );
        }
    }

    public function deleteMenu() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );

        import( '@.WechatMenu.MenuDbControl' );
        

        $menuControl = new MenuDbControl( session( "ecid" ) );

        $code = $menuControl->delMenuItem( $_POST["id"] );
        $this->ajaxReturn( $code, Error::getErrMsg( $code ) , 0 );
    }
}
?>
