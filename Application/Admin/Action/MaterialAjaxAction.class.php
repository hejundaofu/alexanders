<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class MaterialAjaxAction extends PublicAction{
    public function ModifyHandle() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $items = $_POST["items"];
        
        if ( $items != '' ) {
            for ( $i = 0;$i<count( $items );$i++ ) {
                if ( $items[$i]['update'] == 1 ) {
                    $img = $items[$i]['image'];
                    if ( strpos( $img, 'm_' ) ) {
                        $arrTmp = explode( 'm_' , $img );
                        $img = $arrTmp[1];
                    }
                    if ( strpos( $img, 's_' ) ) {
                        $arrTmp = explode( 's_' , $img );
                        $img = $arrTmp[1];
                    }
                    $data = array(
                        'id' => $items[$i]['id'],
                        'title' => $items[$i]['title'],
                        'modifyTime' => date( "Y-m-d H:i:s" ),
                        'modifyUserId' => session($this->_userCfg['UID']) , 
                        'description' => $items[$i]['description'],
                        'content' => $items[$i]['content'],
                        'bigImg' => __ROOT__.'/Public/Uploads/Material/m_' . $img,
                        'minImg' => __ROOT__.'/Public/Uploads/Material/s_' . $img
                    );
                    M( "Company_news" )->data( $data )->save();
                }

                if ( $items[$i]['update'] == 2 ) {
                    $data = array(
                        'ecid' => session($this->_userCfg['ECID']) , 
                        'title' => $items[$i]['title'],
                        'modifyTime' => date( "Y-m-d H:i:s" ),
                        'modifyUserId' => session($this->_userCfg['UID']) , 
                        'description' => $items[$i]['description'],
                        'content' => $items[$i]['content'],
                        'bigImg' => __ROOT__.'/Public/Uploads/Material/m_' . $items[$i]['image'],
                        'minImg' => __ROOT__.'/Public/Uploads/Material/s_' . $items[$i]['image']
                    );
                    $items[$i]['id'] = M( "Company_news" )->data( $data )->add();

                }

                if ( $_POST['delIds'] != '' ) {
                    $arrIds = explode( '-' , $_POST['delIds'] );

                    for ( $i=0; $i<count( $arrIds ); $i++ ) {
                        if ( $arrIds[$i] != '' ) {
                            M( "Company_news" )->delete( $arrIds[$i] );
                        }
                    }
                }
            }

            $materialIds = $items[0]['id'];
            for ( $i=1; $i<count( $items ); $i++ ) {
                $materialIds .= ',' . $items[$i]['id'];
            }
            $data = array(
                'id' => $_POST['groupid'],
                'ecid' => session($this->_userCfg['ECID']) , 
                'modifyTime' => date( "Y-m-d H:i:s" ),
                'modifyUserId' => session($this->_userCfg['UID']) , 
                'materialId' => $materialIds
            );
            $t = M( "Company_material_group" )->data( $data )->save();

            $this->ajaxReturn(Error::SUCCESS_OK);
        }
    }

    public function deleteGroup() {
        if ( !IS_POST ) _404 ( '页面不存在' , U( 'index' ) );
        

        $newsid = M( "company_".session( "ecid" )."_material_group" )->where( "id=".$_POST["id"] )->select();
        $newsidArr = explode( ",", $newsid[0]["materialID"] );

        for ( $i = 0;$i < count( $newsidArr );$i++ ) {
            M( "company_".session( "ecid" )."_material" )->where( "id=".$newsidArr[$i] )->delete();
        }

        if ( M( "company_".session( "ecid" )."_material_group" )->where( "id=".$_POST["id"] )->delete() ) {
            $result["status"] = Error::SUCCESS_OK;
        }else{
            $result["status"] = Error::ERROR_DELETE_HANDLE_ERR;
            $result["info"] = Error::getErrMsg(Error::ERROR_DELETE_HANDLE_ERR);
        }
        
        $this->ajaxReturn($result , "JSON");
    }
}
?>
