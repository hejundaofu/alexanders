<?php
namespace Api\Controller;
use Api\Auth\Auth;
use Think\Controller\RestController;
use Org\Error\Error;
class FwController extends RestController {
    Public function applyFw() {
		switch ($this->_method){
			case 'get': // get请求处理代码
				break;
			case 'put': // put请求处理代码
				break;
			case 'post': // post请求处理代码
				$ecid = I('get.ecid');
				$token = I('get.token');

				if(Auth::checkToken($token, $ecid)){
					$data = json_decode(file_get_contents("php://input"), true);
					$num = $data['num'];
					$order = $data['order'];

					$fwCode = new \Api\Fw\FwCode($ecid);
					$result = $fwCode -> applyFwCode($num, $order);

					$this->response($result,'json');
				}
				else{
					$req = array(
						'errcode' => Error::ERROR_API_DISTRUST_USER,
						'errmsg'  => Error::getErrMsg(Error::ERROR_API_DISTRUST_USER));

					$this->response($req,'json');
				}
				break;
    	}
	}

	public function fwCount(){
		switch ($this->_method){
			case 'get': // get请求处理代码
				$ecid = I('get.ecid');
				$token = I('get.token');
				if(Auth::checkToken($token, $ecid)){

					$fwCode = new \Api\Fw\FwCode($ecid);
					$result = $fwCode -> getCount();

					$this->response($result,'json');
				}
				else{
					$req = array(
						'errcode' => Error::ERROR_API_DISTRUST_USER,
						'errmsg'  => Error::getErrMsg(Error::ERROR_API_DISTRUST_USER));

					$this->response($req,'json');
				}
				break;
			case 'put': // put请求处理代码
				break;
			case 'post': // post请求处理代码
				break;
    	}
	}

	/**
	 * 存储防伪码的扩展信息
	 * post
	 */
	public function exData(){
		switch ($this->_method){
			case 'get': // get请求处理代码
				break;
			case 'put': // put请求处理代码
				break;
			case 'post': // post请求处理代码
				$ecid = I('get.ecid');
				$token = I('get.token');

				if(Auth::checkToken($token, $ecid)){
					$data = json_decode(file_get_contents("php://input"), true);
					$fwCode = $data['fwCode'];
					$exData = $data['exData'];

					$result = array();

					if($fwCode == '' || strlen($fwCode) != 20){
						$result['code'] = -1;
						$result['msg'] = '防伪码格式错误！';
					}
					else if($exData == ''){
						$result['code'] = -1;
						$result['msg'] = '扩展信息格式错误！';
					}else{
						$exControl = new \Api\Fw\ExData($ecid, $fwCode);

						if($exControl->saveExData($exData)){
							$result['code'] = 0;
							$result['msg'] = '保存成功！';
						}else{
							$result['code'] = -1;
							$result['msg'] = '保存失败！';
						}
					}

					$this->response($result,'json');
				}
				else{
					$req = array(
						'errcode' => Error::ERROR_API_DISTRUST_USER,
						'errmsg'  => Error::getErrMsg(Error::ERROR_API_DISTRUST_USER));

					$this->response($req,'json');
				}
				break;
    	}
	}
	public function getCodenum(){
		switch ($this->_method){
			case 'get': // get请求处理代码
				break;
			case 'put': // put请求处理代码
				break;
			case 'post': // post请求处理代码
				$ecid = I('get.ecid');
				$token = I('get.token');
				$date = I('get.time');

				if(Auth::checkToken($token, $ecid)){
					$fwnum= new \Api\Fw\ExData($ecid,'');
					$res=$fwnum->getNum($date);

					if($res){
						$result['code'] = 0;
						$result['msg'] = $res;
					}else{
						$result['code'] = -1;
						$result['msg'] = '失败！';
					}
					$this->response($result,'json');
				}
				else{
					$req = array(
						'errcode' => Error::ERROR_API_DISTRUST_USER,
						'errmsg'  => Error::getErrMsg(Error::ERROR_API_DISTRUST_USER));

					$this->response($req,'json');
				}
				break;
    	}
    }

/**
* 定义函数
*
* 函数功能描述
* 
* @access public
* @param string $ecid
* @param string $arr
* @return integer|string
* @auth XXX <邮箱>
* 修改历史： 1、 曾波 2014-08-26 创建函数
*/

    public function certificatesearch(){
    	switch ($this->_method){
			case 'get': // get请求处理代码
				break;
			case 'put': // put请求处理代码
				break;
			case 'post': // post请求处理代码
				$ecid   =I('get.ecid');
				$arr    =I('get.arr');
				$squery = new \Api\Fw\ExData($ecid,'');
				$num    =$squery->getquery($arr);
				if($num){
					$result['code'] = 0;
					$result['msg'] = '上传成功!';
				}else{
					$result['code'] = -1;
					$result['msg'] = '上传失败！';
				}
				$this->response($result,'json');
				break;
		}
    }
}
?>