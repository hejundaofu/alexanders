<?php
namespace Api\Controller;
use Think\Controller;
use Org\Error\Error;
use Org\Weixin\Wechat;

class CrontabController extends Controller {
    public function sendWxMsg(){
    	if(true || $_SERVER['HTTP_HOST'] == '127.0.0.1'){
    		$items = $this->getSendList();
            
    		foreach ($items as $item)
			{
				$this->sendOneMsg($item);
			}
    	}
    }

    private function getSendList(){
    	$m = M('Send_crontab');

    	$opt['sendTime'] = array('EXP' , 'IS NULL');
    	$opt['times'] = array('LT',2);

    	return $m->where($opt)->select(); 
    }

    private function sendOneMsg($item){
    	$ecid_cache = 'company_info'.$item['ecid'];

    	if(!($companyInfo = S($ecid_cache))){
    		$companyInfo = $this->getCompanyInfo($item['ecid']);
    	}

    	if(!$companyInfo)
    		return false;

    	$options = array(
 			'token'=>'tokenaccesskey', //填写你设定的key
 			'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
 			'appsecret'=>$companyInfo['weixin_AppSecret'] //填写高级调用功能的密钥
 		);

 		$weObj = new Wechat( $options ); 

 		$msg = json_decode($item['content'], true);
        switch ($item['type']) {
            case '':
            case 'custom':
                if($weObj->sendCustomMessage($msg)){
                    $item['sendTime'] = date('Y-m-d H:i:s');
                }
                break;
            case 'templete':
                if($weObj->sendTemplateMessage($msg)){
                    $item['sendTime'] = date('Y-m-d H:i:s');
                }
                break;
            default:
                break;
        }

        $item['times']++;

        $m = M('Send_crontab');
        return $m->save($item);
    }

    private function getCompanyInfo($ecid){
    	$ecid_cache = 'company_info'.$item['ecid'];

    	$m = M( 'View_company_weixin_services' );
        $opt['ecid'] = $ecid;
        $result = $m->where($opt)->find();

        if($result){
            S($ecid_cache, $result);
        }
        
        return $result;
    }
}