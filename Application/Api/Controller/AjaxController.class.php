<?php
namespace Api\Controller;
use Think\Controller;
class AjaxController extends Controller {
    public function shareHandle(){
    	$params = I('post.');
    	$params['time'] = date('Y-m-d H:i:s');

    	$m = M('News_view_log');

    	$m->add($params);

    	$this->ajaxReturn(0, 0, 0);
    }
}
?>