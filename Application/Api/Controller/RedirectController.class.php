<?php
namespace Api\Controller;
use Think\Controller;
use Org\Error\Error;
use Org\Weixin\Wechat;
use Api\Redirect\NewsRedirect;
use Api\Redirect\QuestionRedirect;
class RedirectController extends Controller {
    public function index(){
    	$companyInfo = $this->getCompanyInfo(888);

        $options = array(
            'token'=>'tokenaccesskey', //填写你设定的key
            'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
            'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
        );

        $weixin = new Wechat($options);

        $userInfo = $weixin->getOauthAccessToken();

        $type = I('get.t');

        if($this->RedirectUrl($userInfo['openid'], 888)){
            switch ($type) {
                case 'shop-index':
                    header("Location: http://www.icalex.com/Mobile/");
                    break;
                case 'shop-product':
                    header("Location: http://www.icalex.com/Mobile/Product");
                    break;
                case 'shop-car':
                    header("Location: http://www.icalex.com/Mobile/Order");
                    break;
                case 'exchangeQrcode':
                    $m = M('Company_pay_qrcode_paylog');

                    $result = $m->find(I('get.id'));

                    if(in_array($userInfo['openid'], C('ADMIN_OPENID'))){

                        if($result && !$result['exchangeSign']){
                            $opt['exchangeSign'] = 1;
                            $opt['exchangeTime'] = date("Y-m-d H:i:s");
                            $opt['exchangeOpenId'] = $userInfo['openid'];
                            $opt['id'] = I('get.id');

                            if($m->save($opt))
                                echo "<h1>success！</h1>";
                        }else{
                            echo "<h1>error!</h1>";
                        }
                    }else{
                        echo "<h1>no permission!</h1>";
                    }
                    break;
                default:
                    header("Location: http://www.icalex.com");
                    break;
            }
            
        }
    }

    private function getCompanyInfo($ecid){
    	$m = M('Company_info');

    	return $m->find($ecid);
    }

    /*
     * 函数：getShopToken
     * 作用：生成商城token
     * @Param:  
     */
	private function getShopToken($timestamp, $openId){
		return sha1('sz12365'.'\0\0\0\0\0\0'.$openId.$timestamp);
	}

    private function RedirectUrl($openId, $ecid){
    	$m = M("Company_{$ecid}_user_info");
        $opt['openId'] = $openId;
        $result = $m->where($opt)->find();

        if($result){
            session('userInfo', $result);
            return true;
        }

        return false;
    }
}