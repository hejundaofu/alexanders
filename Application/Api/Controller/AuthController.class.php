<?php
namespace Api\Controller;
use Think\Controller\RestController;
use Org\Error\Error;
class AuthController extends RestController {
    Public function token() {
		switch ($this->_method){
			case 'get': // get请求处理代码
				$data['ecid']       = I('get.ecid');
				$data['grant_type'] = I('get.grant_type');
				$data['appid']      = I('get.appid');
				$data['appsecret']  = I('get.appsecret');

				$auth = new \Api\Auth\Auth($data);
				$req = $auth->token();

				$this->response($req,'json');
				break;
			case 'put': // put请求处理代码
			case 'post': // post请求处理代码
				$req = array(
					'errcode' => Error::ERROR_API_DISTRUST_REQUEST,
					'errmsg' => Error::getErrMsg(Error::ERROR_API_DISTRUST_REQUEST));
				$this->response($data,'json');
				break;
     }
   }
}