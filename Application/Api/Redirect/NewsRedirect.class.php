<?php
namespace Api\Redirect;
use Org\Error\Error;
class NewsRedirect{
	private $_params;
	private $ecid;
	private $fromOpenId;
	private $newsId;
	private $type;
	private $openId;
	private $shareTimes;

	private $base_url = 'http://www.msa12365.com/Index/news/id/<id>/openid/<openid>/ecid/<ecid>/r/<rand>';

	public function __construct($options){
		$this->_params    = $options;
		
		$this->ecid       = isset($options['ecid'])?$options['ecid']:'';
		$this->fromOpenId = isset($options['fromOpenId'])?$options['fromOpenId']:'';
		$this->newsId     = isset($options['newsId'])?$options['newsId']:'';
		$this->type       = isset($options['from_type'])?$options['from_type']:'';
		$this->openId     = isset($options['openId'])?$options['openId']:'';
		$this->shareTimes = isset($options['times'])?$options['times']:'';
	}

	public function getRedirectUrl(){
		$this->saveLog();

		$this->_params['rand'] = rand();

		S('news_view_'.$this->_params['rand'], $this->_params, 5);

		$this->base_url = str_replace('<rand>', $this->_params['rand'], $this->base_url);
		$this->base_url = str_replace('<openid>', $this->openId, $this->base_url);
		$this->base_url = str_replace('<ecid>', $this->ecid, $this->base_url);

		return str_replace('<id>', $this->newsId, $this->base_url);
	}

	private function saveLog(){
		$m = M('News_view_log');

		$opt['ecid']   = $this->ecid;
		$opt['fromOpenId'] = $this->openId;
		$opt['newsId'] = $this->newsId;
		$opt['type']   = $this->type;
		$opt['openId'] = $this->openId;
		$opt['time']   = date('Y-m-d H:i:s');

		if($this->shareTimes)
			$opt['shareTimes'] = $this->shareTimes;

		return $m->add($opt);
	}
}
?>