<?php
namespace Api\Redirect;
use Org\Error\Error;
class QuestionRedirect{
	private $_params;
	private $ecid;
	private $activityId;
	private $type;
	private $openId;

	private $base_url = 'http://www.msa12365.com/index.php/Home/MobileShow/questionActivity/activityId/<activityId>/ecid/<ecid>';

	public function __construct($options){
		$this->_params = $options;
		
		$this->ecid = isset($options['ecid'])?$options['ecid']:'';
		$this->activityId = isset($options['activityId'])?$options['activityId']:'';
		$this->type = isset($options['type'])?$options['type']:'';
		$this->openId = isset($options['openId'])?$options['openId']:'';
	}

	/**
    * 定义函数
    * getQuestionUrl
    * 函数功能描述
    * 获取问题活动的链接
    * @access private
    * @param int $activityId 活动id
    * @auth 范小宝 <fanxb@gbarcode.com>
    * 修改历史： 1、 范小宝 2014-08-06 创建函数
    */
	public function getQuestionUrl(){
		S('question_get_params' , $this->_params);

		$this->base_url = str_replace('<activityId>', $this->activityId, $this->base_url);
		return str_replace('<ecid>', $this->ecid, $this->base_url);
	}
}
?>