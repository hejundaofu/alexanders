<?php
namespace Api\Fw;
use Org\Error\Error;
class FwCode{
	private $ecid;

	public function __construct($ecid){
		$this->ecid = $ecid;
	}

	public function applyFwCode($num, $orderId){
		return $this->popFwCodeFromTxt($num, $orderId);
	}

	public function getCount(){
		$req['num'] = $this->getFwCountFromTxt();
		return $req;
	}

	/**
     * 通过Txt文件获取指定数量的防伪码
     * @param $num int 要获取的数量
     * @param $orderId string 订单编号
     * @return string 返回防伪码
     */
    private function popFwCodeFromTxt($num, $orderId){
    	$txtPath = "./Public/fwCode/{$this->ecid}/{$this->ecid}.txt";
    	$tmpPath = "./Public/fwCode/{$this->ecid}/tmp_{$this->ecid}.txt";

		$file = fopen($txtPath, "r") or exit("Unable to open file!");
		$f2  = fopen($tmpPath, 'w');//建立临时文件
        $result = array(
			'num'  => 0,
			'code' => array());

        $line = '';
        while(!feof($file))
		{
			$str = fgets($file);
			if($result['num'] < $num){
				$line .= $str;
				$fw['fwCode'] = str_replace("\r\n","",$str);
				$fw['sn'] = '';
				$result['orderId'] = $orderId;
			    $result['code'][$result['num']]= $fw;
			    $result['num']++;
			}
			else
			    fputs($f2,$str);

		}
        fclose($file);
        fclose($f2);
        rename($tmpPath,$txtPath);
        file_put_contents("./Public/fwCode/{$this->ecid}/{$orderId}.txt",$line);

        return $result;
    }

    private function getFwCountFromTxt(){
    	$file = fopen("./Public/fwCode/{$this->ecid}/{$this->ecid}.txt", "r") or exit("Unable to open file!");
    	$count = 0;
    	while(!feof($file))
		{
			$str = fgets($file);
			if($str != '')
				$count++;
		}

		fclose($file);

		return $count;
    }

}
?>