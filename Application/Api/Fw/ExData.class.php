<?php
namespace Api\Fw;
use Org\Error\Error;
class ExData{
	private $ecid;
	private $fwCode;
	private $table;
	private $dbPdo = 'mysql://gbarcode:tytm2011@gbarcode.mysql.rds.aliyuncs.com/client_data';

	public function __construct($ecid, $fwCode){
		$this->ecid = $ecid;
		$this->fwCode = $fwCode;

		$this->table = $ecid . '_exdata';
	}

	public function saveExData($exData){
		$m = M($this->table, 'sz12365_fw_', $this->dbPdo);

		$data = $this->isDataExist();

		if($data){
			foreach ($exData as $k => $v) {
			    $data[$k] = $v;
			}

			$m->where("id = ".$data['id'])->save($data);
			return $data['id'];
		}else{
			$opt['fwCode'] = $this->fwCode;
			foreach ($exData as $k => $v) {
			    $opt[$k] = $v;
			}

			$result = $m->add($opt);

			return $result;
		}

		return false;

	}

	private function isDataExist(){
		$m = M($this->table, 'sz12365_fw_', $this->dbPdo);

		$result = $m->where("fwCode = '{$this->fwCode}'")->find();


		return $result;
	}

	public function getNum($date){
		$opt['date']=$date; 
		$m = M('Statistics', 'sz12365_fw_', $this->dbPdo);
		$res=$m->where($opt)->find();
		return $res['num'];
	}

	public function getquery($arr){
    	if(strlen($arr) == 20){
    		$opt['fwCode'] = $arr;
    	}
    	if(substr($arr,0,1)=='S'||substr($arr,0,1)=='s'){
    		$opt['testNo']=$arr;
    	}
    	$m = M($this->table, 'sz12365_fw_', $this->dbPdo);
    	$res = $m->where($opt)->find();
    	if($res){
    		return true;
    	}else{
    		return false;
    	}
    }
}
?>