<?php
namespace Mobile\Controller;
use Think\Controller;
class OrderController extends Controller {
    public function index(){
        $m = M('Company_product');
        $opt['type'] = 'item';
        $opt['brandId'] = 120;
        $opt['enable'] = 1;

        $result = $m->where($opt)->select();
        $userInfo = session('userInfo'); 
        if(!$userInfo)
            $userInfo = \Mobile\Gereral\User::getUserInfo();

        if($userInfo)
            $userId = $userInfo['id'];
        else
            $userId = 0;

        $this->assign('userId', $userId);

        if(session('orderInfo')){
            $tmp = session('orderInfo');
            $tmp['userId'] = $userId;

            session('orderInfo', $tmp);
        }
        $this->assign('shopInfo', $orderInfo = session('orderInfo'));
        $this->assign('products', $result);
        $this->display();
    }

    public function packageChoose(){
        $m = M('Company_product');
        $opt['type'] = 'package';
        $opt['brandId'] = 120;
        $opt['enable'] = 1;
        $result = $m->where($opt)->order('sort asc')->select();
        $this->assign('package', $result);
        $this->display();
    }

    public function newOrder(){
        if($packageId = I('get.pId')){
            $package = $this->getProductInfo($packageId);
            $package['num'] = 1;

            $orderInfo['items'] = array($package);
            $orderInfo['userId'] = session('userInfo')['id'];
            $orderInfo['count'] = 1;
            $orderInfo['prices'] = $package['price'];
            $orderInfo['type'] = 'package';
            $orderInfo['discount'] = 0;
            $orderInfo['payPrice'] = $package['price'];

            session('orderInfo', $orderInfo);
        }

        if(!$orderInfo){
            $orderInfo = session('orderInfo');
        }
        
        $this->assign('city', $this->getCity());
    	$this->assign('orderInfo', $orderInfo);
        $this->assign('address', $this->getUserAddress($orderInfo['userId']));
        $this->display();
    }

    public function getDistrictHandle(){
        $city = I('post.city');

        $m = M('Company_city');
        $opt['city'] = $city;

        $result = $m->where($opt)->find();

        if($result){
            $district = explode(',', $result['district']);

            $data['district'] = $district;
            $data['info'] = $result['info'];

            $this->ajaxReturn($data, 'JSON');
        }

        $this->ajaxReturn(-1, 'JSON');
    }

    private function getCity(){
        $m = M('Company_city');

        return $m->select();
    }

    private function getProductInfo($id){
        $m = M('Company_product');

        return $m->find($id);
    }

    public function addressEdit(){
    	$this->display();
    }

    public function comformOrder(){
        if(!session('orderId')){
            $this->redirect('Index/index');
        }
        $m = M('Company_order');
        $result = $m->find(session('orderId'));
        if($result){
            $result['address'] = unserialize($result['address']);
            $result['goods'] = unserialize($result['goods']);
            $this->assign('orderInfo', $result);
            $this->assign('card', $this->getUserCard());
            $this->display();
        }
    }

    public function getUserCard(){
        $userInfo = session('userInfo');
        if(!$userInfo)
            return false;

        $m = M('Company_card');
        $opt['openid'] = $userInfo['openId'];
        $opt['consume'] = 0;

        $result = $m->where($opt)->select();

        return $result;
    }

    public function finishOrder(){
        $m = M('Company_order');
        $result = $m->find(session('orderId'));

        if($result){
            $msg = '';
            switch($result['sign']){
                case 0:
                    $msg = '购买成功';
                    break;
                case 1:
                    $msg = '购买成功';
                default: 
                    break;
            }

            $this->sendOrderMsg($result);
            $this->sendAdminMsg($result);

            session('orderInfo', null);
            session('orderId', null);

            $this->assign('msg', $msg);
            $this->assign('orderInfo', $result);
        }else{
            $this->assign('msg', '错误访问');
        }

        $this->display();
    }

    public function choosePackage(){
        $id = I('post.id');

        $m = M('Company_product_package');

        $result = $m->find($id);

        if($result){
            $items = unserialize($result['items']);

            $orderInfo = array(
                'count' => 0,
                'discount' => 0,
                'prices' => 0,
                'payPrice' => 0,
                'items' => array()
                );

            $userInfo = session('userInfo');

            if($userInfo)
                $orderInfo['userId'] = $userInfo['id'];
            else
                $orderInfo['userId'] = 0;

            for($i=0; $i<count($items); $i++){
                $m = M('Company_product');
                $result = $m->find($items[$i]['id']);
                $orderInfo['items'][$i] = array(
                    'id' => $result['id'],
                    'discount' => $result['discount'],
                    'img' => $result['minImg'],
                    'name' => $result['name'],
                    'price' => $result['price'],
                    'num' => $items[$i]['num']
                    );

                $box = intval($items[$i]['num']%8);
                if($box > 0 && $result['discount']>0){
                    $orderInfo['discount'] += $result['discount'] * $box;
                }

                $orderInfo['count'] += $items[$i]['num'];
                $orderInfo['prices'] += $result['price']*$items[$i]['num'];
                $orderInfo['payPrice'] = $orderInfo['prices'] - $orderInfo['count'];
            }

            session('orderInfo',$orderInfo);

            $req['Code'] = 0;

            $this->ajaxReturn($req,'JSON');
        }else{
            $req['Code'] = -1;

            $this->ajaxReturn($req,'JSON');
        }
    }

    public function getWxPayParams(){
        $orderInfo = I('post.');
        $m = M('Company_info');

        $companyInfo = $m->find(888);

        $options = array(
            'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
            'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
            'partnerid'=>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
            'partnerkey'=>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
            'paysignkey'=>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
        );
        $wechat = new \Org\Weixin\Wechat($options);

        $info = "Alexander's官网订单";

        $package = $wechat->createPackage($orderInfo['orderNo'],$info, $orderInfo['payPrice']*100, 'http://www.icalex.com/Weixin/Pay/notify', '127.0.0.1');

        $timestamp = time();
        $nonceStr = $wechat->generateNonceStr();

        $paySign = $wechat->getPaySign($package, $timestamp, $nonceStr);

        $reqArr['appId'] = $companyInfo['weixin_AppId'];
        $reqArr['timeStamp'] = "{$timestamp}";
        $reqArr['nonceStr'] = $nonceStr;
        $reqArr['package'] = $package;
        $reqArr['signType'] = 'sha1';
        $reqArr['paySign'] = $paySign;

        $this->ajaxReturn($reqArr, "JSON");
    }

    public function saveOrderSession(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));
    	$orderInfo = I('post.');

    	session('orderInfo',$orderInfo);

    	$data['code'] = 0;

    	$this->ajaxReturn($data, 'json', 0);
    }

    public function addAddress(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));

        $opt = I('post.');

        unset($opt['area']);
        unset($opt['plot']);

        $m = M('Company_user_address');
        $result = $m->add($opt);

        $req['Code'] = $result;

        $this->ajaxReturn($req, 'json', 0);
    }

    public function createOrder(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));

        $orderInfo = I('post.');

        $opt['uId'] = $orderInfo['uId'];
        $opt['price'] = (int)$orderInfo['totalPrice'];
        $opt['address'] = json_encode($orderInfo['address']);
        $opt['goods'] = json_encode($orderInfo['goods']);
        $opt['sendDay'] = $orderInfo['sendDay'];
        $opt['sendTime'] = $orderInfo['sendTime'];
        $opt['goodsTotal'] = $orderInfo['total'];
        $opt['addTime'] = date('Y-m-d H:i:s');
        $opt['sign'] = 0;
        $opt['orderNo'] = $this->createOrderNo();
        $m = M('Company_order');
        $result = $m->add($opt);

        $req['Code'] = $result;

        $this->ajaxReturn($req, 'json', 0);
    }

    public function comformOrderHandle(){
        if(!IS_POST) _404 ('页面不存在' , U('index'));

        $orderInfo = I('post.');

        if($orderInfo['payCardId'] > 0){
            $card = M('Company_card');

            $cardOpt['id'] = $orderInfo['payCardId'];
            $cardOpt['consume'] = 1;
            $cardOpt['useTime'] = date('Y-m-d H:i:s');
            $cardOpt['orderNo'] = $orderInfo['orderNo'];
            $cardOpt['ipaddress'] = get_client_ip();

            $card->save($cardOpt);
        }

        $opt['payType'] = $orderInfo['payType'];
        $opt['id'] = $orderInfo['id'];
        $opt['Message'] = $orderInfo['Message'];
        $opt['payPrice'] = $orderInfo['payPrice'];

        if($opt['payType'] == 'nopay')
            $opt['sign'] = 1;

        $m = M('Company_order');
        $result = $m->save($opt);

        $req['Code'] = 0;

        $this->ajaxReturn($req, 'json', 0);
    }

    public function finishOrderHandle(){
        $orderInfo = I('post.');

        $req = $this->checkOrder($orderInfo);

        if($req['Code'] == 0){
            $code = $this->saveOrder($orderInfo);

            if($code > 0){
                session('orderId', $code);

                $req['Code'] = 0;
                $req['Message'] = '成功！';
            }
            else{
                $req['Code'] = -2;
                $req['Message'] = '保存订单失败！';
            }
        }

        $this->ajaxReturn($req, 'JSON');
    }

    private function checkOrder($orderInfo){
        $result['Code'] = 0;
        $result['Message'] = '成功';

        if($orderInfo['address'] == ''){
            $result['Code'] = 10001;
            $result['Message'] = '您未填写地址信息';
        }else if($orderInfo['address']['address'] == ''){
            $result['Code'] = 10002;
            $result['Message'] = '地址信息为空';
        }else if($orderInfo['address']['name'] == ''){
            $result['Code'] = 10003;
            $result['Message'] = '收货人信息为空';
        }else if(!preg_match("/^1[34578]\d{9}$/", $orderInfo['address']['tel'])){
            $result['Code'] = 10004;
            $result['Message'] = '收货人手机信息有误，请输入手机号';
        }else if($orderInfo['address']['city'] == ''){
            $result['Code'] = 10005;
            $result['Message'] = '配送城市未选择';
        }else if($orderInfo['address']['district'] == ''){
            $result['Code'] = 10006;
            $result['Message'] = '配送地级区域未选择';
        }else if($orderInfo['time'] == ''){
            $result['Code'] = 10007;
            $result['Message'] = '配送时间未选择';
        }else if($orderInfo['time']['date'] == ''){
            $result['Code'] = 10008;
            $result['Message'] = '配送日期未选择';
        }else if($orderInfo['time']['section'] == ''){
            $result['Code'] = 10009;
            $result['Message'] = '配送时间段未选择';
        }

        return $result;
    }

    private function saveOrder($orderInfo){
        $m = M('Company_order');

        $opt['orderNo'] = $this->createOrderNo();
        $opt['uId'] = $orderInfo['userId'];
        $opt['address'] = serialize($orderInfo['address']);
        $opt['goods'] = serialize($orderInfo['items']);
        $opt['price'] = $orderInfo['prices'];
        $opt['payPrice'] = $orderInfo['payPrice'];
        $opt['discount'] = $orderInfo['discount'];
        $opt['goodsTotal'] = $orderInfo['count'];
        $opt['payType'] = $orderInfo['payment'];
        $opt['sendDay'] = $orderInfo['time']['date'];
        $opt['sendTime'] = $orderInfo['time']['section'];
        $opt['addTime'] = date('Y-m-d H:i:s');
        $opt['sign'] = 0;
        $opt['Message'] = $orderInfo['msg'];
        $opt['type'] = $orderInfo['type'];
        $opt['city'] = $orderInfo['address']['city'];

        if($opt['type']){
            $opt['ownerName'] = $orderInfo['OwnerName'];
            $opt['ownerTel'] = $orderInfo['OwnTel'];
        }

        $result = $m->add($opt);

        return $result;
    }

    private function createOrderNo(){
        $m = M('Company_order');

        $result = $m->count();

        $id = $result + 1;

        $pre = sprintf('%02d', $id / 14000000);// 每1400万的前缀
        // 这里乘以 123456789 一是一看就知道是9位长度，二则是产生的数字比较乱便于隐蔽
        $tempcode = sprintf('%09d', sin(($id % 14000000 + 1) / 10000000.0) * 123456789);   
        $seq = '371482506';// 这里定义 0-8 九个数字用于打乱得到的code
        $code = '';
        for ($i = 0; $i < 9; $i++)
        {
            $code .= $tempcode[ $seq[$i] ];
        }
        return '8'.$pre.$code;
    }

    private function sendAdminMsg($orderInfo){
        $userInfo = session('userInfo');

        $goods = unserialize($orderInfo['goods']);
        for($i=0; $i<count($goods); $i++){
            if($i>0)
                $goodsText .= ',';
            $goodsText .= $goods[$i]['name']. '*' . $goods[$i]['num'];
        }

        $address = unserialize(($orderInfo['address']));

        $title = "有新的订单";

        if($userInfo){
            $companyInfo = $this->getCompanyInfo();

            $options = array(
                'appid'      =>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
                'appsecret'  =>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
                'partnerid'  =>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
                'partnerkey' =>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
                'paysignkey' =>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
            );

            $wechat = new \Org\Weixin\Wechat($options);

            $adminUser = $this->getAdminOpenId($address['city']);

            $payType = '货到付款';

            if($orderInfo['payType'] == 'wechatpay')
                $payType = '微信已支付';

            for($i=0; $i<count($adminUser); $i++){
                if($adminUser[$i]['adminOpenId'] == '')
                    continue;

                $openidArr = explode(',', $adminUser[$i]['adminOpenId']);

                for($j=0; $j<count($openidArr); $j++){
                    $data = array(
                        'touser' => $openidArr[$j],
                        'template_id' => '5dmLrSTxUOZARkWyvj2y-zNl1NkNjh0zWwf012xAYJk',
                        'url' => '',
                        'topcolor' => '#000000',
                        'data' => array(
                            'first' => array(
                                'value' => $title,
                                "color"=>"#173177"
                                ),
                            'keyword1' => array(
                                'value' => $orderInfo['orderNo'],
                                "color"=>"#173177"
                                ),
                            'keyword2' => array(
                                'value' => "{$address['name']} : {$address['tel']}",
                                "color"=>"#173177"
                                ),
                            'keyword3' => array(
                                'value' => $goodsText,
                                "color"=>"#173177"
                                ),
                            'keyword4' => array(
                                'value' => $address['address'],
                                "color"=>"#173177"
                                ),
                            'keyword5' => array(
                                'value' => "{$orderInfo['sendDay']}  {$orderInfo['sendTime']}",
                                "color"=>"#173177"
                                ),
                            'remark' => array(
                                'value' => "用户付款方式：【{$payType}】,总金额：【{$orderInfo['payPrice']}元】，用名留言信息：【{$orderInfo['Message']}】,请您及时处理订单",
                                "color"=>"#173177"
                                )
                            )
                        );
                    $wechat->sendTemplateMessage($data);
                }
            }
        }
    }

    private function getAdminOpenId($city){
        $m = M('User_info');

        $result = $m->where("city='{$city}' OR city IS NULL")->select();

        return $result;
    }

    private function sendOrderMsg($orderInfo){
        $userInfo = session('userInfo');

        $goods = unserialize($orderInfo['goods']);
        for($i=0; $i<count($goods); $i++){
            if($i>0)
                $goodsText .= ',';
            $goodsText .= $goods[$i]['name']. '*' . $goods[$i]['num'];
        }

        $address = unserialize(($orderInfo['address']));

        if($orderInfo['sign'] == 1)
            $title = "您好，你的订单【{$orderInfo['orderNo']}】已成功提交，您的预约配送时间为【{$orderInfo['sendDay']} {$orderInfo['sendTime']}】,我们将尽快处理。";
        else
            $title = "您好，你的订单【{$orderInfo['orderNo']}】已成功提交，您的预约配送时间为【{$orderInfo['sendDay']} {$orderInfo['sendTime']}】,我们将尽快处理。";

        if($userInfo){
            $companyInfo = $this->getCompanyInfo();

            $options = array(
                'appid'      =>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
                'appsecret'  =>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
                'partnerid'  =>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
                'partnerkey' =>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
                'paysignkey' =>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
            );

            $data = array(
                'touser' => $userInfo['openId'],
                'template_id' => 'ffjlR69y7HeuQj1JsuGbZGDfNCOACC9q1LglPHLfduc',
                'url' => '',
                'topcolor' => '#000000',
                'data' => array(
                    'first' => array(
                        'value' => $title,
                        "color"=>"#173177"
                        ),
                    'keyword1' => array(
                        'value' => date('Y-m-d H:i:s'),
                        "color"=>"#173177"
                        ),
                    'keyword2' => array(
                        'value' => $orderInfo['payPrice'].'元',
                        "color"=>"#173177"
                        ),
                    'keyword3' => array(
                        'value' => $goodsText,
                        "color"=>"#173177"
                        ),
                    'keyword4' => array(
                        'value' => $address['tel'],
                        "color"=>"#173177"
                        ),
                    'keyword5' => array(
                        'value' => $address['address'],
                        "color"=>"#173177"
                        ),
                    'remark' => array(
                        'value' => "请确认收件人电话、配送时间、地址是否正确，如有疑问请拨打客服电话4009908355或添加微信客服（深圳）icalexsz或微信客服（广州）icalexgz。",
                        "color"=>"#173177"
                        )
                    )
                );
            $wechat = new \Org\Weixin\Wechat($options);
            $wechat->sendTemplateMessage($data);
        }
    }

    private function getCompanyInfo(){
        $m = M('Company_info');

        return $m->find(888);
    }

    private function getUserAddress($uId){
        $m = M('Company_user_address');

        $opt['uId'] = $uId;

        $result = $m->where($opt)->select();

        return $result;
    }
}