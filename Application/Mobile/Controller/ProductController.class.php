<?php
namespace Mobile\Controller;
use Think\Controller;
class ProductController extends Controller {
    public function index(){
    	$userInfo = \Mobile\Gereral\User::getUserInfo();
        if($userInfo)
            $userId = $userInfo['id'];
        else
            $userId = 0;

    	$m = M('Company_product');
        $opt['brandId'] = 120;
        $opt['enable'] = 1;
    	$result = $m->where($opt)->order('type desc,id')->select();

    	$this->assign('userId', $userId);
    	$this->assign('productInfo', $result);
        $this->display();
    }

    public function item(){
    	$id = I('get.id');

    	$userInfo = session('userInfo');
        if($userInfo)
            $userId = $userInfo['id'];
        else
            $userId = 0;

    	$m = M('Company_product');
    	$result = $m->find($id);
    	$this->assign('userId', $userId);
    	$this->assign('productInfo', $result);
    	$this->display();
    }
}