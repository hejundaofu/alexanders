<?php
namespace Mobile\Controller;
use Think\Controller;
class UserController extends Controller {
    public function index(){
    	$userInfo = session('userInfo'); 
        if(!$userInfo){
        	if(I('get.openid')){
	            $userInfo = $this->getUserInfo(I('get.openid'));
	            session('userInfo', $userInfo);   
	        }else{
	            $userInfo = \Mobile\Gereral\User::getUserInfo();   
	        }
        }

        $this->assign('userInfo', $userInfo);
        $this->theme('unify')->display();
    }

    public function info(){
    	$userInfo = session('userInfo'); 
        if(!$userInfo){
        	if(I('get.openid')){
	            $userInfo = $this->getUserInfo(I('get.openid'));
	            session('userInfo', $userInfo);   
	        }else{
	            $userInfo = \Mobile\Gereral\User::getUserInfo();   
	        }
        }

        $this->assign('userInfo', $userInfo);
        $this->theme('unify')->display();
    }

    private function getUserInfo($openId){
        $m = M('Company_888_user_info');

        $opt['openId'] = $openId;

        return $m->where($opt)->find();
    }
}