<?php
namespace Mobile\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
        //测试专用
        if(I('get.openid')){
            $userInfo = $this->getUserInfo(I('get.openid'));
            session('userInfo', $userInfo);   
        }else{
            $userInfo = \Mobile\Gereral\User::getUserInfo();   
        }
        
        if($userInfo)
            $userId = $userInfo['id'];
        else
            $userId = 0;

        $this->assign('userId', $userId);
        $this->display();
    }

    public function wechatLogin(){
    	$qrCodeSense = I('get.sense');
    	$openid = I('get.openid');

    	$m = M('Company_888_user_info');
    	$opt['openId'] = $openid;

    	$result = $m->where($opt)->find();

    	$this->assign('sense', $qrCodeSense);
    	$this->assign('userInfo', $result);
    	$this->display();
    }

    private function getUserInfo($openId){
        $m = M('Company_888_user_info');

        $opt['openId'] = $openId;

        return $m->where($opt)->find();
    }
}