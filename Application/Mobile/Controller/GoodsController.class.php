<?php
namespace Mobile\Controller;
use Think\Controller;
class GoodsController extends Controller {
    public function addGoods(){
        $this->display();
    }

    public function chooseAllGoods(){
    	$m = M('Company_product');

    	$opt['brandId'] = 120;
        $opt['enable'] = 1;
        $opt['type'] = 'item';

    	$result = $m->where($opt)->select();

    	$this->assign('goods', $result);
    	$this->assign('type', I('post.type'));
    	$this->assign('itemId', I('post.id'));

    	$this->display();
    }

    public function goodsList(){
    	$m = M('Company_product');

    	$opt['brandId'] = 120;
        $opt['enable'] = 1;

    	$result = $m->where($opt)->select();

    	$this->assign('goods', $result);
    	$this->display();
    }
}