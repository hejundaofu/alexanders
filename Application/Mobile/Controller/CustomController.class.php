<?php
namespace Mobile\Controller;
use Think\Controller;
class CustomController extends Controller {
    public function index(){
    	$type = I('get.type');
    	$id = I('get.id');

    	if($type == 'product'){
    		$goodsInfo = $this->getProductInfo($id);

    		if($goodsInfo){
    			$goodsArr = array();
    			for($i=0; $i<8; $i++){
    				$goodsArr[$i] = $goodsInfo[0];
    			}
    			$this->assign('goodInfo', $goodsArr);
    			$this->assign('total', count($goodsArr));
    		}
    		else{
    			$this->assign('total', 0);
    		}
    	}
        $this->assign('userInfo', session('userInfo'));
        $this->display();
    }

    private function getProductInfo($id){
    	$m = M('Company_product');
    	$result = $m->select($id);

    	return $result;
    }
}