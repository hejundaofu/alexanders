<?php
namespace Mobile\Gereral;
use Org\Weixin\Wechat;
class User {
    public static function getUserInfo(){
        // if($userInfo = session('userInfo')){
        //     return $userInfo;
        // }

        $companyInfo = self::getCompanyInfo();

        $options = array(
            'token'=>'tokenaccesskey', //填写你设定的key
            'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
            'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
        );

        $weixin = new Wechat($options);

        if($userInfo = $weixin->getOauthAccessToken()){
            $m = M("Company_888_user_info");
            $opt['openId'] = $userInfo['openid'];
            $result = $m->where($opt)->find();

            if($result){
                session('userInfo', $result);
                return $result;
            }else{
                $id = $m->add($opt);
                $opt['id'] = $id;
                session('userInfo', $opt);
                return $opt;
            }
        }

        if(I('get.redirct') == 1)
            return '';

        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'] . '&redirct=1';

        $url = urlencode($url);

        $redirctUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$companyInfo['weixin_AppId']}&redirect_uri={$url}&response_type=code&scope=snsapi_base&state=0#wechat_redirect";

        header("Location: {$redirctUrl}");
    }

    public static function getCompanyInfo(){
        $m = M('Company_info');

        return $m->find(888);
    }
}
?>