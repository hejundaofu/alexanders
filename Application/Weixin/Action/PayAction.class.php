<?php
// 本类由系统自动生成，仅供测试用途
namespace Weixin\Action;
use Think\Action;
use Org\Error\Error;
use Org\Weixin\Wechat;
class PayAction extends Action {
	public function getPackage(){
		$weObj = new Wechat( $this->getOption() ); 

		$wechatMsg = $weObj->getRev()->getRevData();

		$keyword = explode('_', $wechatMsg['ProductId']);

		$package = '';

		switch ($keyword[0]) {
			case 'order':
				$orderInfo = $this->getOrder($keyword[1]);

				$info = "Alexander's官网订单";

				$package = $weObj->createPackage($orderInfo['orderNo'],$info, $orderInfo['payPrice']*100, 'http://www.icalex.com/Weixin/Pay/notify', '127.0.0.1');

				break;
			case 'pay':
				$payQrInfo = $this->getPayQrInfo($keyword[1]);

				$info = $payQrInfo['name'];

				$orderNo = 
				$package = $weObj->createPackage($this->createOrderNo().'_'.$keyword[1],$info, $payQrInfo['price']*100, 'http://www.icalex.com/Weixin/Pay/qrcodeNotify', '127.0.0.1');
				break;
			default:
				# code...
				break;
		}

		$weObj->nativePay($package)->reply();
	}

	public function notify(){
		$weObj = new Wechat( $this->getOption() ); 

		if($weObj->checkOrderSignature()){
			$params = I('get.');

			if($params['trade_state'] == 0){
				$opt['orderNo'] = $params['out_trade_no'];
				$m = M('Company_order');

				$sav['sign'] = 1;
				$sav['payTime'] = date('Y-m-d H:i:s');
				$sav['payParams'] = serialize($params);

				$m->where($opt)->save($sav);

				S('pay_'.$opt['orderNo'], 1, 600);

				echo 'success';
			}		
		}
	}

	public function qrcodeNotify(){
		$weObj = new Wechat( $this->getOption() ); 

		if($weObj->checkOrderSignature()){
			$params = I('get.');
			$wecatMsg = $weObj->getRev()->getRevData();

			$this->log(2);

			if($params['trade_state'] == 0){
				$m = M('Company_pay_qrcode_paylog');

				$keyword = explode('_', $params['out_trade_no']);

				$save['payQrcodeId'] = $keyword[1];
				$save['openId'] = $wecatMsg['OpenId'];
				$save['payTime'] = date('Y-m-d H:i:s');
				$save['payInfo'] = serialize($params);

				$logId = $m->where($opt)->add($save);
				if($wecatMsg['IsSubscribe'] == 1){
					$this->sendOrderMsg($save['payQrcodeId'], $wecatMsg['OpenId'], $logId);
				}
			}

			echo 'success';
		}
	}

	public function test(){
		$this->sendOrderMsg(1, 'ooKmYt8VGB2V3l3ZXolpFbTukE58', 65);
	}

	private function sendOrderMsg($payQrcodeId, $openid, $logId){
        $m = M('Company_pay_qrcode');

        $result = $m->find($payQrcodeId);

        $title = "您好，你的订单【{$result['name']}】已成功支付，请到工作人员处领取。";

        $companyInfo = $this->getCompanyInfo();

        $options = array(
            'appid'      =>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
            'appsecret'  =>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
            'partnerid'  =>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
            'partnerkey' =>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
            'paysignkey' =>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
        );

        $data = array(
            'touser' => $openid,
            'template_id' => '9vrZ54E2usz08SVYjManqdUlHlI--x3eMqWdXKBMwpI',
            'url' => "http://www.icalex.com/Mobile/Exchange/payQrcode/openid/{$openid}/id/{$logId}",
            'topcolor' => '#000000',
            'data' => array(
                'first' => array(
                    'value' => $title,
                    "color"=>"#173177"
                    ),
                'keyword1' => array(
                    'value' => date('Y-m-d H:i:s'),
                    "color"=>"#173177"
                    ),
                'keyword2' => array(
                    'value' => $result['price'].'元',
                    "color"=>"#173177"
                    ),
                'keyword3' => array(
                    'value' => '',
                    "color"=>"#173177"
                    ),
                'keyword4' => array(
                    'value' => '',
                    "color"=>"#173177"
                    ),
                'remark' => array(
                    'value' => "如有疑问，请拨打客服电话4009908355或在微信留言。！",
                    "color"=>"#173177"
                    )
                )
            );
        $wechat = new \Org\Weixin\Wechat($options);
        $wechat->sendTemplateMessage($data);
    }


	private function createOrderNo(){
        $id = time();

        $pre = sprintf('%02d', $id / 14000000);// 每1400万的前缀
        // 这里乘以 123456789 一是一看就知道是9位长度，二则是产生的数字比较乱便于隐蔽
        $tempcode = sprintf('%09d', sin(($id % 14000000 + 1) / 10000000.0) * 123456789);   
        $seq = '371482506';// 这里定义 0-8 九个数字用于打乱得到的code
        $code = '';
        for ($i = 0; $i < 9; $i++)
        {
            $code .= $tempcode[ $seq[$i] ];
        }
        return $pre.$code;
    }

	private function getPayQrInfo($id){
		$m = M('Company_pay_qrcode');

		return $m->find($id);
	}

	private function getOrder($orderNo){
		$m = M('Company_order');

		$opt['orderNo'] = $orderNo;

		return $m->where($opt)->find();
	}

	private function getOption(){
		$companyInfo = $this->getCompanyInfo();

		$option = array(
			'appid'=>$companyInfo['weixin_AppId'], //填写高级调用功能的app id
            'appsecret'=>$companyInfo['weixin_AppSecret'], //填写高级调用功能的密钥
            'partnerid'=>$companyInfo['weixin_PartnerID'], //财付通商户身份标识
            'partnerkey'=>$companyInfo['weixin_PartnerKey'], //财付通商户权限密钥Key
            'paysignkey'=>$companyInfo['weixin_PaySignKey'] //商户签名密钥Key
			);

		return $option;
	}

	private function log($text){
		$filename = 'file.txt';

		$fh = fopen($filename, "a");
		fwrite($fh, $text."\r\n");
		fclose($fh);
	}

	private function getCompanyInfo(){
		$m = M('Company_info');

        return $m->find(888);
	}
}