<?php
namespace Weixin\Dealer;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class DealerResponse extends Response {
    private $dealerInfo;
    private $response = array();

    public function __construct( $companyInfo, $wechatMsg ) {
        $this->companyInfo = $companyInfo;
        $this->wechatMsg = $wechatMsg;
    }

    /**
     * getResponse获取经销商回复
     * @author:范小宝
     * @return array
     */
    public function getResponseWithId($dealerId , $type){
        //通过dealerId获取经销商信息
        $m = M("Company_dealers");
        $opt['id'] = $dealerId;
        $this->dealerInfo = $m->where($opt)->find();

        array_push($this->response , array(
            'Title' => "经销商：" . $this->dealerInfo['name'] , 
            'PicUrl' => $this->getDealerPic() , 
            'Description' => "" ,
            'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/dealer/id/{$dealerId}" 
            ));

        if($type == 'all' || $type == 'dealer'){
            //判断经销商类型
            if($this->dealerInfo['root'] == -1){
                //获取下级代理商
                $this->getUnRootDealer();
            }else{
                //获取上级代理商
                $this->getRootDealer();
            }
        }

        if($type == 'all' || $type == 'web'){
            $this->getDealerWeb();
        }

        return $this->response;
    }

    /**
     * getUnRootDealer获取下级经销商
     * @author:范小宝
     * @return array
     */
    private function  getUnRootDealer(){
        $m = M("Company_dealers");
        $opt['root'] = $this->dealerInfo['id'];
        $item = $m->where($opt)->select();

        for($i = 0;$i<count($item);$i++){
            array_push($this->response , array(
                'Title' => "下级经销商：" . $item[$i]['name'] , 
                'PicUrl' => $this->getImgUrl($item[$i]['minImg']) , 
                'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/dealer/id/".$item[$i]['id']
                ));
        }

        return;
    }

    /**
     * getUnRootDealer获取上级经销商
     * @author:范小宝
     * @return array
     */
    private function getRootDealer(){
        $m = M("Company_dealers");
        $opt['id'] = $this->dealerInfo['root'];
        $rootDealer = $m->where($opt)->find();
        array_push($this->response , array(
            'Title' => "所属总代理：" . $rootDealer['name'] , 
            'PicUrl' => $this->getImgUrl($rootDealer['minImg']) , 
            'Url' => WechatConst::SERVER_DOMAIN . "/index.php/MobileShow/dealer/id/".$rootDealer['id']
            ));

        return;
    }

    /**
     * getDealerWeb获取经销商网店
     * @author:范小宝
     * @return array
     */
    private function getDealerWeb(){
        $itemNum = count($this->response);
        if($itemNum < 8){
            //判断是否绑定网店
            $m = M("Company_dealers_electricity");
            $opt = null;
            $opt['dealerId'] = $this->dealerInfo['id'];
            $web = $m->where($opt)->limit(8 - $itemNum)->select();
            if($web){
                for($i = 0;$i<count($web);$i++){
                    array_push($this->response , array(
                        'Title' => "经销商网店：" . $web[$i]['name'] , 
                        'PicUrl' => $this->getImgUrl($this->dealerInfo['minImg']) , 
                        'Url' => $web[$i]['url']
                        ));
                }
            }
        }

        return;
    }

    private function getDealerPic(){
        if($this->dealerInfo['bigImg'] == ''){
            return $this->getImgUrl($this->dealerInfo['bigImg']);
        }else{
            $m = M("Company_info");
            $opt['company_ecid'] = $this->companyInfo['ecid'];
            $result = $m->where($opt)->find();

            return $this->getImgUrl($result['company_logo']);
        }
    }
}
?>