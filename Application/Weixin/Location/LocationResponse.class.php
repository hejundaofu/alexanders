<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Weixin\Location;
use Weixin\Response\Response;
use Weixin\Response\WechatConst;
class LocationResponse {

    private $ecid;      //厂商ID
    private $location_X;    //地理位置纬度
    private $location_Y;    //地理位置经度
    private $scale;         //地图缩放比例
    private $label;         //地理位置信息

    const BAIDUMAP_DATABOX = 32406;
    const BAIDUMAP_AK = '41193a426878963512af9f91de8b414f';

    public function __construct( $ecid, $location ) {
        $this->ecid = $ecid;
        $this->location_X = $location['x'];
        $this->location_Y = $location['y'];
        $this->scale = $location['scale'];
        $this->label = $location['label'];
    }

    public function getResponse() {
        $response = $this->searchNearbyFromBaiduMapResponse();

        if ( $response->body != null ) {
            $total = $response->body->total;
            return $this->getWechatResponse( $total, $response->body->contents );
        }

    }

    /*
     *  函数：searchNearbyFromBaiduMapResponse
     *  作用：从百度地图API接口获取附近的店
     */
    private function searchNearbyFromBaiduMapResponse( $word = '', $radius=2000 ) {
        $url = "http://api.map.baidu.com/geosearch/v2/nearby?";
        $url .= 'location='. $this->location_Y . ','. $this->location_X;
        $url .= '&geotable_id=' . self::BAIDUMAP_DATABOX;
        $url .= '&q=' . $word;
        $url .= '&radius=' . $radius;
        $url .= '&filter=ecid|' . $this->ecid . ',' . $this->ecid;
        $url .= '&ak=' . self::BAIDUMAP_AK;

        Vendor( 'Unirest.Unirest' );
        return \Unirest::get( $url, array( "Accept" => "application/json" ) );
    }

    /*
     * 函数：getWechatResponse
     * 作用：将回复的内容转化为微信所需的参数
     * @Param:  $total   总个数
     *          $content    门店详细列表数组
     */
    private function getWechatResponse( $total, $content ) {
        //回复类型为news
        $reply['type'] = 'news';

        //如果只有一个地址记录，则附加图文素材
        if ( $total == 1 ) {
            $reply['content'][0] = array(
                'Title'       =>$content[0]->title . "\n距离您" . $content[0]->distance . '米',
                'Description' =>'',
                'PicUrl'      => WechatConst::SERVER_DOMAIN . $this->getImg( $content[0]->uid, true ),
                'Url'         => WechatConst::SERVER_DOMAIN );


            return $reply;
        }

        //当有多个店时返回多个，最多返回6个
        if ( $total > 1 ) {
            for ( $i=0; $i<$total; $i++ ) {
                if ( $i >= 6 )
                    break;
                $reply['content'][$i] = array(
                    'Title'       =>$content[$i]->title . "\n距离您" . $content[$i]->distance . '米',
                    'Description' =>'',
                    'PicUrl'      => WechatConst::SERVER_DOMAIN . ( $i == 0 )?$this->getImg( $content[$i]['uid'], true ):$this->getImg( $content[$i]['uid'] ),
                    'Url'         => WechatConst::SERVER_DOMAIN );
            }
            return $reply;
        }
    }

    private function getImg( $poiID, $bBig = false ) {
        $m = M( 'Company_location_set' );

        $data['poiID'] = $poiID;
        $result = $m->where( $data )->find();

        if ( $result ) {
            if ( bBig )
                return $result['bigImg'];
            else
                return $result['smallImg'];
        }
    }
}
?>
