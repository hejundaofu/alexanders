<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Lucky
 *
 * @author lisha_000
 */
namespace Weixin\Lucky;
class Lucky {
    //put your code here
    private $luckyID;       //项目ID
    private $luckyName;     //抽奖项目名
    private $ecid;          //厂商ID
    private $userID;        //用户
    private $hasLuckyProject;      //是否有抽奖活动
    private $num;           //本次查询数量
    private $luckyNum;      //幸运数字
    private $isLucky;       //是否是中奖人
    private $message;       //中奖提示
    private $exchangeCode;  //对奖号码
    private $img;           //图片
    private $prize;         //奖品
    private $fwCode;        //防伪码
    private $isMultipleLucky;   //是否倍数幸运数字
    private $multipleBase;      //基础倍数

    public function __construct( $time, $ecid, $userID, $product, $fwCode ) {
        $this->ecid = $ecid;
        $this->userID = $userID;
        $this->fwCode = $fwCode;
        $m = M( 'Company_'. $this->ecid . '_lucky' );

        $data['startTime'] = array( 'ELT', $time );
        $data['endTime'] = array( 'EGT', $time );
        $data['bindProduct'] = array( 'LIKE', array('%'.$product.'%', 'all'), 'OR' );


        $m->startTrans();
        $result = $m->lock( true )->where( $data )->find();

        if ( $result == null ) {
            $this->hasLuckyProject = false;
            $m->commit();
            return;
        }
        $this->hasLuckyProject = true;
        $this->luckyName = $result['lucky_Name'];
        $this->num = $result['total'] + 1;
        $this->luckyNum = $result['lucky_Num'];
        $this->luckyID = $result['id'];
        $this->img = $result['img'];
        $this->isMultipleLucky = $result['isMultipleLucky'];
        $this->multipleBase = $result['MultipleBase'];

        $addDate['id'] = $result['id'];
        $addDate['total'] = $this->num;

        if ( $this->num == $this->luckyNum && $result['prize_Num'] > $result['lucky_Count'] ) {
            //绑定中奖号码
            $this->exchangeCode = $this->bindExchangeCode();

            if ( $this->exchangeCode != '' ) {
                $this->isLucky = true;
                $this->message = "感谢您参与《" .$this->luckyName. "》微信抽奖活动，恭喜您获得“" . $this->prize. "”，点击了解详情。";
            }
            else {
                $this->isLucky = false;
                $this->message = "感谢您参与《" .$this->luckyName. "》微信抽奖活动，本次防伪验证未中奖，点击了解活动详情。";
            }

            $addDate['lucky_Count'] = $result['lucky_Count'] + 1;

            //设置下一个中奖数字
            if ( $addDate['lucky_Count'] != $result['prize_Num'] )
                $addDate['lucky_Num'] = $this->updateLuckyNum( $result['startTime'], $result['endTime'], $this->num, $result['prize_Num'] - $result['lucky_Count'] );
        }
        else {
            $this->isLucky = false;
            $this->message = "感谢您参与《" .$this->luckyName. "》微信抽奖活动，本次防伪验证未中奖，点击了解活动详情。";
        }

        $m->save( $addDate );
        $m->commit();
    }

    public function hasLucky() {
        return $this->hasLuckyProject;
    }

    public function isLucky() {
        return $this->isLucky;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getluckyID() {
        return $this->luckyID;
    }

    public function getImg() {
        return $this->img;
    }

    public function getPrize() {
        return $this->prize;
    }
    /**
     * 绑定中奖号码
     */
    private function bindExchangeCode() {
        $m = M( 'Company_'. $this->ecid . '_lucky_exchange' );

        $data['luckyID'] = $this->luckyID;
        $data['wechatID'] = array( 'EXP', 'IS NULL' );

        $maxID = $m->where( $data )->max( 'id' );

        $randID = (int)$maxID * $this->randomFloat();

        $data['id'] =array( 'EGT', $randID );

        $m->startTrans();
        $result = $m->lock( true )->where( $data )->find();

        if ( $result != null ) {
            $updateData['id'] = $result['id'];
            $updateData['wechatID'] = $this->userID;
            $updateData['fwCode'] = $this->fwCode;
            $updateData['luckyTime'] = date("Y-m-d H:i:s",time());
            $m->save( $updateData );
            $m->commit();

            $this->prize = $result['prize'];
            return $result['luckyCode'];
        }
    }

    /**
     *
     *
     * @param string  $startTime  开始时间
     * @param string  $endTime    结束时间
     * @param int     $totalCheck 已查人数
     * @param int     $luckyCount 剩余奖品
     */
    private function updateLuckyNum( $startTime, $endTime, $totalCheck, $luckyCount ) {
        $nextLucky = 0;

        if ( $this->isMultipleLucky == 1 ) {
            $nextLucky = $this->luckyNum + $this->multipleBase;
        }
        else {
            $nowdate = strtotime( date( "Y-m-d" ) );
            $startdate = strtotime( $startTime );
            $enddate = strtotime( $endTime );

            $passDays = round( ( $nowdate-$startdate )/3600/24 );
            $haveDays = round( ( $enddate-$nowdate )/3600/24 );
            $nextLucky = $totalCheck + round( $totalCheck / $passDays * $haveDays / $luckyCount );
        }

        if ( $nextLucky <= $this->luckyNum ) {
            $nextLucky = $this->luckyNum * 2;
        }

        return $nextLucky;
    }

    private function randomFloat( $min = 0, $max = 1 ) {
        return $min + mt_rand() / mt_getrandmax() * ( $max - $min );
    }
}

?>
