/*
Navicat MySQL Data Transfer

Source Server         : 阿里云
Source Server Version : 50518
Source Host           : gbarcode.mysql.rds.aliyuncs.com:3306
Source Database       : db_fw

Target Server Type    : MYSQL
Target Server Version : 50518
File Encoding         : 65001

Date: 2014-07-07 15:16:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sz12365_fw_web_config
-- ----------------------------
DROP TABLE IF EXISTS `sz12365_fw_web_config`;
CREATE TABLE `sz12365_fw_web_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  `modifyUserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
